<div class="control-group">
    <label class="control-label">Автор</label>
    <div class="controls">

        <label class="radio">
            <input type="radio" value="1" name="select_author" <?php if($n->trainer_id!=null) echo "checked='checked'"?>>
            <select name='trainer'>
                <?php foreach($trainers as $t) {?>
                <option value='<?php echo $t->id?>' <?php if(($n->trainer_id!=null) and ($n->trainer_id==$t->id)) echo "selected='selected'"?>><?php echo $t->name?></option>
                <?php }?>
            </select>
        </label>
        <label class="radio">
            <input type="radio" value="2" name="select_author" <?php if($n->author!=null) echo "checked='checked'"?>>
            <input type="text" name="author" value="<?php if($n->author!=null) echo $n->author?>">
        </label>
    </div>

</div>