/**
 * Created with JetBrains PhpStorm.
 * User: Ahineya
 * Date: 22.01.13
 * Time: 13:27
 */

$(function() {
    $('textarea.editor').tinymce({
        // Location of TinyMCE script
        script_url : '/assets/js/libs/tiny_mce.js',

        // General options
        theme : "advanced",

        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline, bullist,numlist",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "/assets/css/style.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Replace values for the template plugin
        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }
    });
});

$('#date_start').datepicker();
$('#date_end').datepicker();