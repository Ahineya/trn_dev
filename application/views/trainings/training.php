<?php
/**
 * @author Pavel Evsegneev
 * Date: 17.01.13
 * Time: 16:54
 */
$tr = $training['training'];
?>
    <div class="row-fluid">
        <div class="span6 offset3">
            <div class="row-fluid">
                <div class="span12">
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a> <span class="divider">/</span></li>
                        <li><a href="/trainings">Тренинги</a> <span class="divider">/</span></li>
                        <li class="active"><?php echo $tr->name;?></li>
                    </ul>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12 training-name">
                    <h3><?php echo $tr->name; ?></h3>
                </div>
            </div>
            <div class="row-fluid training-name">
                <div class="span6 training-company">
                    <div class="header">
                        Организатор
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <img src = "http://placehold.it/100x100" alt="">
                            <span class=left15'><a href="/companies/<?php echo $tr->company_id;?>"><?php echo str_replace(" ","&nbsp;",$tr->company_name); ?></a></span>
                            <hr>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            Документ об окончании: <?php echo ($tr->document=="")?"Нет":$tr->document; ?>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            Стоимость: <?php echo ($tr->price=="0")?"Бесплатно!":$tr->price." грн."; ?>
                        </div>
                    </div>

                </div>
                <div class="span6 training-trainers">
                    <div class='header'>Тренеры</div>
                    <?php
                        foreach($training['trainers'] as $t) {
                    ?>
                        <div class="row-fluid trainer-mini-block">
                            <div class="span12">
                                <img src = "http://placehold.it/75x100" alt="">
                                <span class='left15'><a href="/trainers/<?php echo $t->trainer_id;?>"><?php echo str_replace(" ","&nbsp;",$t->name); ?></a></span>
                            </div>
                        </div>
                    <?php }?>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span6 offset6">
                        <div class=" header">
                            Место и дата
                        </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <?php echo date("d.m.Y",$tr->date_start);?> &mdash; <?php echo date("d.m.Y",$tr->date_end);?>
                            , <?php echo $tr->city." ".$tr->place?>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($tr->audience!="") {?>
            <div class="row-fluid">
                <div class="span12">
                    <div class="header-part">
                        Целевая аудитория
                    </div>
                    <div class="part">
                        <?php echo $tr->audience;?>
                    </div>
                </div>
            </div>
            <?php }?>

            <?php if ($tr->goal!="") {?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="header-part">
                            Цель
                        </div>
                        <div class="part">
                            <?php echo $tr->goal;?>
                        </div>
                    </div>
                </div>
            <?php }?>

            <?php if ($tr->description!="") {?>
            <div class="row-fluid">
                <div class="span12">
                    <div class="header-part">
                        Описание
                    </div>
                    <div class="part">
                        <?php echo $tr->description;?>
                    </div>
                </div>
            </div>
            <?php }?>

            <?php if ($tr->additional!="") {?>
            <div class="row-fluid">
                <div class="span12">
                    <div class="header-part">
                        Дополнительная информация
                    </div>
                    <div class="part">
                        <?php echo $tr->additional;?>
                    </div>
                </div>
            </div>
            <?php }?>

            <div class="row-fluid">
                <div class="span12 contact-information">
                    <div>
                        Контактная информация<hr>
                    </div>
                    <div>
                        Компания: <a><?php echo $tr->company_name;?></a>
                    </div>
                    <div>
                        Контактное лицо: <?php echo $tr->contact;?>
                    </div>
                    <div>
                        Телефон: <?php echo $tr->phone;?>
                    </div>
                </div>
            </div>


        </div>
    </div>
