<?php
/**
 * @author Pavel Evsegneev
 * Date: 17.01.13
 * Time: 14:04
 */


?>
<div class='row-fluid'>
    <div class="span8 offset2">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Тренинг</th>
                <th>Компания</th>
                <th>Место, дата</th>
                <th>Стоимость</th>
            </tr>
            </thead>
            <tbody>
    <?php
        foreach ($trainings as $t) {
    ?>
            <tr>
                <td>
                    <a href="<?php echo "/trainings/".$t->id;?>"><?php echo $t->name; ?></a>
                    <div>
                        <?php echo $t->description;?>
                    </div>
                </td>
                <td><a href="/companies/<?php echo $t->company_id;?>"><?php echo $t->company_name; ?></a></td >
                <td >
                    <p><?php echo $t->city; ?> <?php echo $t->place; ?></p>
                    <p><?php echo date("d.m.Y",$t->date_start);?> &mdash; <?php echo date("d.m.Y",$t->date_end);?></p>
                </td >
                <td ><?php echo $t->price; ?> грн.</td >
            </tr >

    <?php
        }
    ?>
            </tbody >
        </table>
    </div>
</div>