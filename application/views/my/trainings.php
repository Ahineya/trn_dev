<?php
/**
 * @author Pavel Evsegneev
 * Date: 25.01.13
 * Time: 12:51
 */
?>

<div class="row-fluid">
    <div class="span6 offset3">
        <table class="table table-condensed">
            <thead>
            <tr>
                <td style="width:75%">Тренинги <?php echo "(",$trainings->count()," шт.)";?></td>
                <td colspan="2"><a href='/my/add/training/'><i class="icon-plus-sign"></i> Добавить тренинг</a></td>
            </tr>
            </thead>
            <tbody>

            <tr class="info">
                <td>Название, тренер</td>
                <td>Место</td>
                <td>Когда</td>
            </tr>
            <?php foreach($trainings as $t) {?>
            <tr>
                <td>
                    <p><?php echo $t->name;?></p>
                    <p><?php echo $t->trainer_name;?></p>
                    <p><a href="#">Изменить</a> | <a href="/my/delete/training/<?php echo $t->id?>">Удалить</a></p>
                </td>
                <td><?php echo $t->city,", ",$t->place;?></td>
                <td><?php echo date("d.m.Y",$t->date_start);?></td>
            </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>