<?php
/**
 * @author Pavel Evsegneev
 * Date: 25.01.13
 * Time: 11:00
 */
?>

<div class="row-fluid">
    <div class="span6 offset3">
        <table class="table table-condensed">
            <thead>
                <tr>
                    <td style="width:75%">Тренинги <?php echo "(",$trainings->count()," шт.)";?></td>
                    <td><a href='/my/add/training/'><i class="icon-plus-sign"></i> Добавить тренинг</a></td>
                </tr>
            </thead>
            <tbody>

                <tr class="info">
                    <td>Название</td>
                    <td>Фигня всякая</td>
                </tr>
                <?php foreach($trainings as $t) {?>
                    <tr>
                        <td>
                            <p><?php echo $t->name;?></p>
                            <p><?php echo $t->trainer_name;?></p>
                            <p><a href="/my/edit/training/<?php echo $t->id?>">Изменить</a> | <a href="/my/delete/training/<?php echo $t->id?>">Удалить</a></p>
                        </td>
                        <td>ag</td>
                    </tr>
                <?php }?>
            </tbody>
        </table>

        <!-- Trainers -->

        <table class="table table-condensed">
            <thead>
            <tr>
                <td style="width:75%">Тренеры <?php echo "(",$trainers->count()," шт.)";?></td>
                <td><a href="/my/add/trainer"><i class="icon-plus-sign"></i> Добавить тренера</a></td>
            </tr>
            </thead>
            <tbody>

            <tr class="info">
                <td>Имя</td>
                <td>Фигня всякая</td>
            </tr>
            <?php foreach($trainers as $t) {?>
            <tr>
                <td>
                    <p><?php echo $t->name;?></p>
                    <p><a href="/my/edit/trainers/<?php echo $t->id?>">Изменить</a> | <a href="/my/delete/trainer/<?php echo $t->id;?>">Удалить</a></p>
                </td>
                <td>ag</td>
            </tr>
                <?php }?>
            </tbody>
        </table>

        <!-- News -->
        <table class="table table-condensed">
            <thead>
            <tr>
                <td style="width:75%">Новости <?php echo "(",$news->count()," шт.)";?></td>
                <td><a href="/my/add/news"><i class="icon-plus-sign"></i> Добавить новость</a></td>
            </tr>
            </thead>
            <tbody>

            <tr class="info">
                <td>Название</td>
                <td>Фигня всякая</td>
            </tr>
            <?php foreach($news as $n) {?>
            <tr>
                <td>
                    <p><?php echo $n->name;?> <?php echo date("d.m.Y",$n->date);?></p>
                    <p><a href="/my/edit/news/<?php echo $n->id?>">Изменить</a> | <a href="/my/delete/news/<?php echo $n->id;?>">Удалить</a></p>
                </td>
                <td>ag</td>
            </tr>
                <?php }?>
            </tbody>
        </table>

        <!-- Articles -->

        <table class="table table-condensed">
            <thead>
            <tr>
                <td style="width:75%"> Статьи <?php echo "(",$articles->count()," шт.)";?></td>
                <td><a href="/my/add/article"><i class="icon-plus-sign"></i> Добавить статью</a></td>
            </tr>
            </thead>
            <tbody>

            <tr class="info">
                <td>Название</td>
                <td>Фигня всякая</td>
            </tr>
            <?php foreach($articles as $a) {?>
            <tr>
                <td>
                    <p><?php echo $a->name;?> <?php echo date("d.m.Y",$a->date);?></p>
                    <p><a href="/my/edit/news/<?php echo $a->id?>">Изменить</a> | <a href="/my/delete/article/<?php echo $a->id;?>">Удалить</a></p>
                </td>
                <td>ag</td>
            </tr>
                <?php }?>
            </tbody>
        </table>

    </div>
</div>