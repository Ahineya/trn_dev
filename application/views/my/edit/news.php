<?php
/**
 * @author Pavel Evsegneev
 * Date: 25.01.13
 * Time: 15:25
 */
$n = $news;
?>
<div class="row-fluid">
    <div class="span6 offset3">

        <form class="form-horizontal" method="post" action="/my/edit/news/<?php echo $nid?>">
            <fieldset>
                <div id="legend" class="">
                    <legend class="">Редактирование новости</legend>
                </div>
                <div class="control-group">
                    <label class="control-label">Статус</label>
                    <div class="controls">
                        <!-- Multiple Radios -->
                        <label class="radio">
                            <input type="radio" value="Доступна на сайте" name="status" <?php if($n->visible==1) echo "checked='checked'"?>">
                            Доступна на сайте
                        </label>
                        <label class="radio">
                            <input type="radio" value="Скрыта" name="status" <?php if($n->visible==0) echo "checked='checked'"?>">
                            Скрыта
                        </label>
                    </div>

                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="caption">Заголовок</label>
                    <div class="controls">
                        <input type="text" placeholder="Заголовок" class="input-xlarge" id="caption" name="caption" value="<?php echo $n->name?>" >
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">Краткая аннотация</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" class="" name="introduction"><?php echo $n->introduction?></textarea>
                        </div>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">Содержание</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" class="editor" name="text"><?php echo $n->text?></textarea>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Картинка</label>

                    <!-- File Upload -->
                    <div class="controls">
                        <input class="input-file" id="fileInput" type="file">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Автор</label>
                    <div class="controls">

                        <label class="radio">
                            <input type="radio" value="1" name="select_author" <?php if($n->trainer_id!=null) echo "checked='checked'"?>>
                            <select name='trainer'>
                                <?php foreach($trainers as $t) {?>
                                <option value='<?php echo $t->id?>' <?php if(($n->trainer_id!=null) and ($n->trainer_id==$t->id)) echo "selected='selected'"?>><?php echo $t->name?></option>
                                <?php }?>
                            </select>
                        </label>
                        <label class="radio">
                            <input type="radio" value="2" name="select_author" <?php if($n->author!=null) echo "checked='checked'"?>>
                            <input type="text" name="author" value="<?php if($n->author!=null) echo $n->author?>">
                        </label>
                    </div>

                </div>

                <div class="control-group">
                    <label class="control-label"></label>

                    <!-- Button -->
                    <div class="controls">
                        <button class="btn btn-default">Редактировать</button>
                    </div>
                </div>

            </fieldset>
        </form>

    </div>
</div>