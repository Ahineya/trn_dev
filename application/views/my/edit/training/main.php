<?php
/**
 * @author Pavel Evsegneev
 * Date: 05.02.13
 * Time: 15:07
 */
$t = $training['training'];
$tra = $training['trainers'];
$trainers = NULL;
$cat = NULL;
?>
<div class="row-fluid">
    <div class="span6 offset3">
        <div class="row-fluid">
            <div class="span6">
                <h4>Общая информация</h4>
            </div>
            <div class="span6 pull-right">
                <button class="btn btn-info">Редактировать</button>
            </div>
        </div>
        <hr>
        <div class="row-fluid">
            <div class="span6">Название тренинга:</div>
            <div class="span6"><?php echo $t->name?></div>
        </div>
        <div class="row-fluid">
            <div class="span6">Активность:</div>
            <div class="span6"><?php echo ($t->visible==1)?'Виден на сайте':'Скрыт' ?></div>
        </div>
        <div class="row-fluid">
            <div class="span6">Тип:</div>
            <div class="span6"><?php echo ($t->type==1)?'Обычный тренинг':'Корпоративный тренинг' ?></div>
        </div>
        <div class="row-fluid">
            <div class="span6">Тренеры:</div>
            <div class="span6">
                <?php foreach($tra as $tr) {
                    $trainers .= $tr->name.", ";
                }
                echo substr($trainers, 0, strlen($trainers)-2);
                ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">Категории: </div>
            <div class="span6">
                <?php foreach($categories as $c) {
                    $cat .= $c->name.", ";
                }
                echo substr($cat, 0, strlen($cat)-2);
                ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">Форма проведения:</div>
            <div class="span6"><?php echo $t->form?></div>
        </div>
        <div class="row-fluid">
            <div class="span6">Стоимость:</div>
            <div class="span6"><?php echo ($t->price=="0")?"Бесплатно!":$t->price." грн."; ?></div>
        </div>
        <div class="row-fluid">
            <div class="span6">Комментарий к стоимости:</div>
            <div class="span6"><?php echo ($t->price_comment)?></div>
        </div>
        <div class="row-fluid">
            <div class="span6">Контактное лицо:</div>
            <div class="span6"><?php echo $t->contact?></div>
        </div>
        <div class="row-fluid">
            <div class="span6">Телефон:</div>
            <div class="span6"><?php echo $t->phone?></div>
        </div>
        <div class="row-fluid">
            <div class="span6">E-mail: </div>
            <div class="span6"><?php echo $t->email?></div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <h4>Описание мероприятия</h4>
            </div>
            <div class="span6 pull-right">
                <button class="btn btn-info">Редактировать</button>
            </div>
        </div>
        <hr>
        <div class="row-fluid">
            <div class="span6">Продолжительность:</div>
            <div class="span6"><?php echo $t->duration?></div>
        </div>
        <div class="row-fluid">
            <div class="span6">Документ об окончании:</div>
            <div class="span6"><?php echo $t->document?></div>
        </div>
        <div class="row-fluid">
            <div class="span6">Целевая аудитория:</div>
            <div class="span6"><?php echo $t->audience?></div>
        </div>
        <div class="row-fluid">
            <div class="span6">Цель мероприятия:</div>
            <div class="span6"><?php echo $t->goal?></div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <p>Программа</p>
                <?php echo $t->description?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <p>Дополнительная информация</p>
                <?php echo $t->additional?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6"></div>
            <div class="span6"></div>
        </div>
        <div class="row-fluid">
            <div class="span6"></div>
            <div class="span6"></div>
        </div>

    </div>
</div>