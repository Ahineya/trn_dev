<?php
/**
 * @author Pavel Evsegneev
 * Date: 31.01.13
 * Time: 11:28
 */
$a = $article;
?>
<div class="row-fluid">
<div class="span6 offset3">

<form class="form-horizontal" method="post" action="/my/edit/article/<?php echo $aid?>">
<fieldset>
<div id="legend" class="">
    <legend class="">Редактирование статьи</legend>
</div>
<div class="control-group">
    <label class="control-label">Статус</label>
    <div class="controls">
        <!-- Multiple Radios -->
        <label class="radio">
            <input type="radio" value="Доступна на сайте" name="status" <?php if($a->visible==1) echo "checked='checked'"?>">
            Доступна на сайте
        </label>
        <label class="radio">
            <input type="radio" value="Скрыта" name="status" <?php if($a->visible==0) echo "checked='checked'"?>>
            Скрыта
        </label>
    </div>

</div>

<div class="control-group">

    <!-- Text input-->
    <label class="control-label" for="caption">Заголовок</label>
    <div class="controls">
        <input type="text" placeholder="Заголовок" class="input-xlarge" id="caption" name="caption" value="<?php echo $a->name?>" >
        <p class="help-block"></p>
    </div>
</div>

<div class="control-group">

    <!-- Textarea -->
    <label class="control-label">Краткая аннотация</label>
    <div class="controls">
        <div class="textarea">
            <textarea type="" class="" name="introduction"><?php echo $a->introduction?></textarea>
        </div>
    </div>
</div>

<div class="control-group">

    <!-- Textarea -->
    <label class="control-label">Содержание</label>
    <div class="controls">
        <div class="textarea">
            <textarea type="" class="editor" name="text"><?php echo $a->description?></textarea>
        </div>
    </div>
</div>

<div class="control-group">
    <label class="control-label">Категории</label>
    <div class="controls checklist categories">

        <?php foreach($categories as $c){?>

        <label class="checkbox">
            <input type="checkbox" value="<?php echo $c['id']?>" name="categories[]"
                <?php
                    foreach($article_cat as $ac) {
                        if($c['id'] == $ac->category_id) echo "checked='checked'";
                    }
                ?>
            >
            <?php echo $c['name']?>
        </label>

        <?php }?>

    </div>
</div>


<div class="control-group">
    <label class="control-label">Картинка</label>

    <!-- File Upload -->
    <div class="controls">
        <input class="input-file" id="fileInput" type="file">
    </div>
</div>

<div class="control-group">
    <label class="control-label">Автор</label>
    <div class="controls">
        <!-- Multiple Radios -->
        <label class="radio">
            <input type="radio" value="1" name="select_author" <?php if($a->trainer_id!=null) echo "checked='checked'"?>>
            <select name='trainer'>
                <?php foreach($trainers as $t) {?>
                <option value='<?php echo $t->id?>' <?php if(($a->trainer_id!=null) and ($a->trainer_id==$t->id)) echo "selected='selected'"?>><?php echo $t->name?></option>
                <?php }?>
            </select>
        </label>
        <label class="radio">
            <input type="radio" value="2" name="select_author" <?php if($a->author!=null) echo "checked='checked'"?>>
            <input type="text" name="author" value="<?php if($a->author!=null) echo $a->author?>">
        </label>
    </div>

</div>

<div class="control-group">
    <label class="control-label"></label>

    <!-- Button -->
    <div class="controls">
        <button class="btn btn-default">Редактировать</button>
    </div>
</div>

</fieldset>
</form>

</div>
</div>