<?php
/**
 * @author Pavel Evsegneev
 * Date: 25.01.13
 * Time: 13:27
 */
?>
<div class="row-fluid">
    <div class="span6 offset3">
        <a href="/my/add/trainer"><i class="icon-plus-sign"></i> Добавить тренера</a>
    <?php
    foreach($trainers as $t) {
    ?>
        <hr>
    <div class="row-fluid">
        <div class="span1 offset2">
            <a href="/trainers/<?php echo $t->id?>"><img src="<?php echo $t->photo;?>" alt=""></a>
        </div>
        <div class="span7">
            <p><?php echo $t->name;?></p>
            <p><a href="mailto:<?php echo $t->email;?>"><?php echo $t->email;?></a></p>
            <p><a href="<?php echo $t->site;?>"><?php echo $t->site;?></a></p>
            <p><a>Изменить</a> | <a href="/my/delete/trainer/<?php echo $t->id;?>">Удалить</a></p>

        </div>

    </div>

<?php }?>
    </div>
</div>