<?php
/**
 * @author Pavel Evsegneev
 * Date: 31.01.13
 * Time: 14:30
 */

?>
<div class="row-fluid">
    <div class="span6 offset3">

        <form class="form-horizontal" method="post" action="/my/add/trainer">
            <fieldset>
                <div id="legend" class="">
                    <legend class="">Добавить тренера</legend>
                </div>
                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="name">Ф.И.О.</label>
                    <div class="controls">
                        <input type="text" placeholder="Только русские буквы" class="input-xlarge" name="name" id="name" required>
                        <p class="help-block">Пример: Фердищенко Акакий Самуилович</p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">О тренере</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" class="editor" name="description"> </textarea>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Фото</label>

                    <!-- File Upload -->
                    <div class="controls">
                        <input class="input-file" id="fileInput" type="file">
                    </div>
                </div><div class="control-group">

                <!-- Text input-->
                <label class="control-label" for="phone">Телефон</label>
                <div class="controls">
                    <input type="text" placeholder="Телефон" class="input-xlarge" name="phone" id="phone">
                    <p class="help-block">Например +380631111111</p>
                </div>
            </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="email">E-mail</label>
                    <div class="controls">
                        <input type="text" placeholder="E-mail" class="input-xlarge" name="email" id="email">
                        <p class="help-block">Пример: test@example.com</p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="site">Сайт</label>
                    <div class="controls">
                        <input type="text" placeholder="http://" class="input-xlarge" name="site" id="site">
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"></label>

                    <!-- Button -->
                    <div class="controls">
                        <button class="btn btn-default">Добавить тренера</button>
                    </div>
                </div>

            </fieldset>
        </form>

    </div>
</div>