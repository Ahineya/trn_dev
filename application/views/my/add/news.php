<?php
/**
 * @author Pavel Evsegneev
 * Date: 25.01.13
 * Time: 15:25
 */

?>
<div class="row-fluid">
    <div class="span6 offset3">

        <form class="form-horizontal" method="post" action="/my/add/news">
            <fieldset>
                <div id="legend" class="">
                    <legend class="">Добавление новости</legend>
                </div>
                <div class="control-group">
                    <label class="control-label">Статус</label>
                    <div class="controls">
                        <!-- Multiple Radios -->
                        <label class="radio">
                            <input type="radio" value="Доступна на сайте" name="status" checked="checked">
                            Доступна на сайте
                        </label>
                        <label class="radio">
                            <input type="radio" value="Скрыта" name="status">
                            Скрыта
                        </label>
                    </div>

                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="caption">Заголовок</label>
                    <div class="controls">
                        <input type="text" placeholder="Заголовок" class="input-xlarge" id="caption" name="caption" <?php if(isset($caption)) {echo "value=".$caption."";}?> >
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">Краткая аннотация</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" class="" name="introduction"><?php if(isset($introduction)) {echo $introduction;}?></textarea>
                        </div>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">Содержание</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" class="editor" name="text"><?php if(isset($description)) {echo $description;}?></textarea>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Картинка</label>

                    <!-- File Upload -->
                    <div class="controls">
                        <input class="input-file" id="fileInput" type="file">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Автор</label>
                    <div class="controls">
                        <!-- Multiple Radios -->
                        <label class="radio">
                            <input type="radio" value="1" name="select_author" checked="checked">
                            <select name='trainer'>
                                <?php foreach($trainers as $t) {?>
                                    <option value='<?php echo $t->id?>'><?php echo $t->name?></option>
                                <?php }?>
                            </select>
                        </label>
                        <label class="radio">
                            <input type="radio" value="2" name="select_author">
                            <input type="text" name="author">
                        </label>
                    </div>

                </div>

                <div class="control-group">
                    <label class="control-label"></label>

                    <!-- Button -->
                    <div class="controls">
                        <button class="btn btn-default">Добавить</button>
                    </div>
                </div>

            </fieldset>
        </form>

    </div>
</div>