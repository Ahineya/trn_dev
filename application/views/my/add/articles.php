<?php
/**
 * @author Pavel Evsegneev
 * Date: 31.01.13
 * Time: 11:28
 */

?>
<div class="row-fluid">
    <div class="span6 offset3">

        <form class="form-horizontal" method="post" action="/my/add/article">
            <fieldset>
                <div id="legend" class="">
                    <legend class="">Добавление статьи</legend>
                </div>
                <div class="control-group">
                    <label class="control-label">Статус</label>
                    <div class="controls">
                        <!-- Multiple Radios -->
                        <label class="radio">
                            <input type="radio" value="Доступна на сайте" name="status" checked="checked">
                            Доступна на сайте
                        </label>
                        <label class="radio">
                            <input type="radio" value="Скрыта" name="status">
                            Скрыта
                        </label>
                    </div>

                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="caption">Заголовок</label>
                    <div class="controls">
                        <input type="text" placeholder="Заголовок" class="input-xlarge" id="caption" name="caption" <?php if(isset($caption)) {echo "value=".$caption."";}?> >
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">Краткая аннотация</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" class="" name="introduction"><?php if(isset($introduction)) {echo $introduction;}?></textarea>
                        </div>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">Содержание</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" class="editor" name="text"><?php if(isset($description)) {echo $description;}?></textarea>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Категории</label>
                    <div class="controls checklist categories">

                        <?php foreach($categories as $c){?>

                        <label class="checkbox">
                            <input type="checkbox" value="<?php echo $c['id']?>" name="categories[]">
                            <?php echo $c['name']?>
                        </label>

                        <?php }?>

                    </div>
                </div>


                <div class="control-group">
                    <label class="control-label">Картинка</label>

                    <!-- File Upload -->
                    <div class="controls">
                        <input class="input-file" id="fileInput" type="file">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Автор</label>
                    <div class="controls">
                        <!-- Multiple Radios -->
                        <label class="radio">
                            <input type="radio" value="1" name="select_author" checked="checked">
                            <select name='trainer'>
                                <?php foreach($trainers as $t) {?>
                                <option value='<?php echo $t->id?>'><?php echo $t->name?></option>
                                <?php }?>
                            </select>
                        </label>
                        <label class="radio">
                            <input type="radio" value="2" name="select_author">
                            <input type="text" name="author">
                        </label>
                    </div>

                </div>



               <!-- <div class="control-group"><label for="chk_1">
                    <input type="checkbox" name="data[cat][1][0]"
                           id="chk_1"
                           value="1" />
                    Банк, финансы</label><label for="chk_2">
                    <input type="checkbox" name="data[cat][1][1]"
                           id="chk_2"
                           value="2" />
                    Бухгалтерия, аудит, налогообложение</label><label for="chk_67">
                    <input type="checkbox" name="data[cat][1][2]"
                           id="chk_67"
                           value="67" />
                    Гостинично-ресторанный бизнес, HoReCa</label><label for="chk_3">
                    <input type="checkbox" name="data[cat][1][3]"
                           id="chk_3"
                           value="3" />
                    Делопроизводство, секретариат, call-центры</label><label for="chk_4">
                    <input type="checkbox" name="data[cat][1][4]"
                           id="chk_4"
                           value="4" />
                    Информационные технологии, IT</label><label for="chk_44">
                    <input type="checkbox" name="data[cat][1][5]"
                           id="chk_44"
                           value="44" />
                    Красота, спорт, здоровье</label><label for="chk_5">
                    <input type="checkbox" name="data[cat][1][6]"
                           id="chk_5"
                           value="5" />
                    Лидерство, командообразование, коучинг</label><label for="chk_6">
                    <input type="checkbox" name="data[cat][1][7]"
                           id="chk_6"
                           value="6" />
                    Личностное развитие, управление временем</label><label for="chk_7">
                    <input type="checkbox" name="data[cat][1][8]"
                           id="chk_7"
                           value="7" />
                    Логистика, таможня, ВЭД</label><label for="chk_47">
                    <input type="checkbox" name="data[cat][1][9]"
                           id="chk_47"
                           value="47" />
                    Медицина, фармацевтика</label><label for="chk_8">
                    <input type="checkbox" name="data[cat][1][10]"
                           id="chk_8"
                           value="8" />
                    Маркетинг, реклама, PR, брендинг</label><label for="chk_9">
                    <input type="checkbox" name="data[cat][1][11]"
                           id="chk_9"
                           value="9" />
                    Менеджмент, управление компанией</label><label for="chk_43">
                    <input type="checkbox" name="data[cat][1][12]"
                           id="chk_43"
                           value="43" />
                    Недвижимость</label><label for="chk_10">
                    <input type="checkbox" name="data[cat][1][13]"
                           id="chk_10"
                           value="10" />
                    Охрана труда, безопасность бизнеса</label><label for="chk_11">
                    <input type="checkbox" name="data[cat][1][14]"
                           id="chk_11"
                           value="11" />
                    Переговоры, коммуникации</label><label for="chk_12">
                    <input type="checkbox" name="data[cat][1][15]"
                           id="chk_12"
                           value="12" />
                    Право, юриспруденция</label><label for="chk_13">
                    <input type="checkbox" name="data[cat][1][16]"
                           id="chk_13"
                           value="13" />
                    Продажи, работа с клиентами</label><label for="chk_127">
                    <input type="checkbox" name="data[cat][1][17]"
                           id="chk_127"
                           value="127" />
                    Промышленность, производство</label><label for="chk_14">
                    <input type="checkbox" name="data[cat][1][18]"
                           id="chk_14"
                           value="14" />
                    Психология, НЛП</label><label for="chk_46">
                    <input type="checkbox" name="data[cat][1][19]"
                           id="chk_46"
                           value="46" />
                    Тренинги для тренеров</label><label for="chk_30">
                    <input type="checkbox" name="data[cat][1][20]"
                           id="chk_30"
                           value="30" />
                    Управление персоналом, HR</label><label for="chk_16">
                    <input type="checkbox" name="data[cat][1][21]"
                           id="chk_16"
                           value="16" />
                    Управление проектами, бизнес-планирование</label><label for="chk_45">
                    <input type="checkbox" name="data[cat][1][22]"
                           id="chk_45"
                           value="45" />
                    Эстетическое образование, культура</label><label for="chk_17">
                    <input type="checkbox" name="data[cat][1][23]"
                           id="chk_17"
                           value="17" />
                    Другое</label>
                </div>-->

                <div class="control-group">
                    <label class="control-label"></label>

                    <!-- Button -->
                    <div class="controls">
                        <button class="btn btn-default">Добавить</button>
                    </div>
                </div>

            </fieldset>
        </form>

    </div>
</div>