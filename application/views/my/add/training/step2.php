<?php
/**
 * @author Pavel Evsegneev
 * Date: 01.02.13
 * Time: 11:53
 */

?>
<div class="row-fluid">
    <div class="span6 offset3">

        <form class="form-horizontal" method="post" <?php echo ($type==1)?'action="/my/add/training/3':'/my/add/training/final';?>">
            <fieldset>
                <div id="legend" class="">
                    <legend class="">Описание мероприятия</legend>
                </div>
                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="duration">Продолжительность</label>
                    <div class="controls">
                        <input type="text" placeholder="" class="input-xlarge" name="duration" id="duration" >
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="document">Документ об окончании</label>
                    <div class="controls">
                        <input type="text" placeholder="" class="input-xlarge" name="document" id="document" >
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">Целевая аудитория</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" class="editor" name="audience" id="audience" > </textarea>
                        </div>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">Цель мероприятия</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" class="editor" name="goal" id="goal" > </textarea>
                        </div>
                    </div>
                </div>

            </fieldset>
            <fieldset>
            <div id="legend2" class="">
                <legend class="">Программа</legend>
            </div>

                <div class="control-group">
                    <!-- Textarea -->
                        <div class="textarea">
                            <textarea type="" class="editor" name="description" id="description" > </textarea>
                        </div>
                </div>

            </fieldset>
            <fieldset>
                <div id="legend3" class="">
                    <legend class="">Дополнительная информация</legend>
                </div>
                <div class="control-group">
                    <!-- Textarea -->
                        <div class="textarea">
                            <textarea type="" class="editor" name="additional" id="additional" > </textarea>
                        </div>
                 </div>

                <div class="control-group">
                    <label class="control-label"></label>

                    <!-- Button -->
                    <div class="controls">

                        <button class="btn btn-info"><?php echo ($type==1)?'Далее':'Сохранить';?></button>
                    </div>
                </div>

            </fieldset>
        </form>

    </div>
</div>