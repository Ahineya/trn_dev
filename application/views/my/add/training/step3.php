<?php
/**
 * @author Pavel Evsegneev
 * Date: 01.02.13
 * Time: 13:59
 */

?>
<div class="row-fluid">
    <div class="span6 offset3">

        <form class="form-horizontal" action="/my/add/training/final" method="post">
            <fieldset>
                <div id="legend" class="">
                    <legend class="">Время проведения</legend>
                </div>
                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="date_start">Дата начала</label>
                    <div class="controls">
                        <input type="text" placeholder="" class="input-xlarge" name="date_start" id="date_start" data-date="<?php echo date("d.m.Y")?>" data-date-format="dd.mm.yyyy" value="<?php echo date("d.m.Y")?>">
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="time_start">Время начала</label>
                    <div class="controls">
                        <input type="text" placeholder="" class="input-xlarge" name="time_start" id="time_start">
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="date_end">Дата окончания</label>
                    <div class="controls">
                        <input type="text" placeholder="" class="input-xlarge" name="date_end" id="date_end"  data-date="<?php echo date("d.m.Y")?>" data-date-format="dd.mm.yyyy" value="<?php echo date("d.m.Y")?>">
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">
                    <!-- Select Basic -->
                    <label class="control-label">Город</label>
                    <div class="controls">
                        <select class="input-xlarge" name='city' required>
                            <?php foreach($cities as $c) { ?>
                            <option value="<?php echo $c['id'];?>"><?php echo $c['name'];?></option>
                            <?php }?>

                        </select>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="address">Адрес</label>
                    <div class="controls">
                        <input type="text" placeholder="" class="input-xlarge" name="address" id="address">
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"></label>

                    <!-- Button -->
                    <div class="controls">
                        <button class="btn btn-success">Сохранить</button>
                    </div>
                </div>

            </fieldset>
        </form>

    </div>
</div>