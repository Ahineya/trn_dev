<?php
/**
 * @author Pavel Evsegneev
 * Date: 01.02.13
 * Time: 11:27
 */

?>
<div class="row-fluid">
    <div class="span6 offset3">

        <form class="form-horizontal" method="post" action="/my/add/training/2">
            <fieldset>
                <div id="legend" class="">
                    <legend class="">Новый тренинг</legend>
                </div>
                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="name">Название</label>
                    <div class="controls">
                        <input type="text" placeholder="" class="input-xlarge" name='name' id='name' value="<?php if(isset($_POST['name'])) echo $_POST['name']?>">
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Тренеры</label>
                    <div class="controls checklist trainers">

                        <label class='checkbox'>
                            <input type="checkbox" value="0" name="trainers[]">
                            Без привязки к тренеру
                        </label>

                        <?php foreach($trainers as $t){?>

                        <label class="checkbox">
                            <input type="checkbox" value="<?php echo $t->id?>" name="trainers[]">
                            <?php echo $t->name?>
                        </label>

                        <?php }?>

                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Категории</label>
                    <div class="controls checklist categories">

                        <?php foreach($categories as $c){?>

                        <label class="checkbox">
                            <input type="checkbox" value="<?php echo $c['id']?>" name="categories[]">
                            <?php echo $c['name']?>
                        </label>

                        <?php }?>

                    </div>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="form">Форма проведения</label>
                    <div class="controls">
                        <input type="text" placeholder="" class="input-xlarge" name="form" id="form" value="<?php if(isset($_POST['form'])) echo $_POST['form']?>">
                        <p class="help-block">Например: тренинг, семинар, конференция</p>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Тип</label>
                    <div class="controls">
                        <!-- Multiple Radios -->
                        <label class="radio">
                            <input type="radio" value="1" name="type" <?php if(isset($_POST['type']) and ($_POST['type']==1)) {echo 'checked="checked"';} else if (!isset($_POST['type'])) {echo 'checked="checked"';} ?>>
                            открытый (есть назначенные даты)
                        </label>
                        <label class="radio">
                            <input type="radio" value="2" name="type" <?php if(isset($_POST['type']) and ($_POST['type']==2)) echo 'checked="checked"'?>>
                            корпоративный (под заказ)
                        </label>
                    </div>

                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="price">Стоимость</label>
                    <div class="controls">
                        <input type="text" placeholder="" class="input-xlarge" name="price" id="price" value="<?php if(isset($_POST['price'])) echo $_POST['price']?>">
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"></label>

                    <!-- Multiple Checkboxes -->
                    <div class="controls">
                        <!-- Inline Checkboxes -->
                        <label class="checkbox inline">
                            <input type="checkbox" value="1" name=is_free>
                            бесплатный тренинг
                        </label>
                    </div>

                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="price_comment">Комментарий к стоимости</label>
                    <div class="controls">
                        <input type="text" placeholder="" class="input-xlarge" name="price_comment" id="price_comment" value="<?php if(isset($_POST['price_comment'])) echo $_POST['price_comment']?>">
                        <p class="help-block">Например: без учёта фуршета</p>
                    </div>
                </div>

                </fieldset>
            <fieldset>
                <div id="legend2" class="">
                    <legend class="">Контактная информация</legend>
                </div>
                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="contact">Контактное лицо</label>
                    <div class="controls">
                        <input type="text" placeholder="" class="input-xlarge" name="contact" id="contact" value="<?php if(isset($_POST['contact'])) echo $_POST['contact']?>">
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="phone">Телефон</label>
                    <div class="controls">
                        <input type="text" placeholder="" class="input-xlarge" name="phone" id="phone" value="<?php if(isset($_POST['phone'])) echo $_POST['phone']?>">
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="email">E-mail</label>
                    <div class="controls">
                        <input type="text" placeholder="" class="input-xlarge" name="email" id="email" value="<?php if(isset($_POST['email'])) echo $_POST['email']?>">
                        <p class="help-block"></p>
                    </div>
                </div>
                </fieldset>
                <fieldset>
                    <div id="legend3" class="">
                        <legend class="">Размещение тренинга</legend>
                    </div>
                <div class="control-group">
                    <label class="control-label">Активность</label>
                    <div class="controls">
                        <!-- Multiple Radios -->
                        <label class="radio">
                            <input type="radio" value="1" name="active" <?php if(isset($_POST['active']) and ($_POST['active']==1)) {echo 'checked="checked"';} else if (!isset($_POST['active'])) {echo 'checked="checked"';} ?>>
                            Доступен на сайте
                        </label>
                        <label class="radio">
                            <input type="radio" value="0" name="active" <?php if(isset($_POST['active']) and ($_POST['active']==0)) echo 'checked="checked"'?>>
                            Виден только вам
                        </label>
                    </div>

                </div>

                <div class="control-group">
                    <label class="control-label"></label>

                    <!-- Button -->
                    <div class="controls">
                        <button class="btn btn-info">Далее</button>
                    </div>
                </div>

            </fieldset>
        </form>

    </div>
</div>