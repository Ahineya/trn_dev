<?php
/**
 * @author Pavel Evsegneev
 * Date: 25.01.13
 * Time: 13:43
 */
?>
<div class="row-fluid">
    <div class="span6 offset3">
        <table class="table table-condensed">
            <thead>
            <tr>
                <td style="width:75%"> Статьи <?php echo "(",$articles->count()," шт.)";?></td>
                <td><a  href="/my/add/article"><i class="icon-plus-sign"></i> Добавить статью</a></td>
            </tr>
            </thead>
            <tbody>

            <tr class="info">
                <td>Название</td>
                <td>Дата</td>
            </tr>
            <?php foreach($articles as $a) {?>
                <tr>
                    <td>
                        <p><?php echo $a->name;?> </p>
                        <p><?php echo ($a->trainer_name!="")?$a->trainer_name:$a->author?></p>
                        <p><a href="/my/edit/news/<?php echo $a->id?>">Изменить</a> | <a href="/my/delete/article/<?php echo $a->id;?>">Удалить</a></p>
                    </td>
                    <td><?php echo date("d.m.Y",$a->date);?></td>
                </tr>
            <?php }?>
            </tbody>
        </table>
    </div>
</div>