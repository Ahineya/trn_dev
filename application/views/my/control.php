<div class="row-fluid"xmlns="http://www.w3.org/1999/html">
    <div class="span6 offset3">
        <ul class="nav nav-pills">
            <li <?php echo ($active=="my")?"class='active'":""?>>
                <a href="/my">Мой раздел</a>
            </li>
            <li <?php echo ($active=="trainings")?"class='active'":""?>><a href="/my/trainings">Тренинги</a></li>
            <li <?php echo ($active=="trainers")?"class='active'":""?>><a href="/my/trainers">Тренеры</a></li>
            <li <?php echo ($active=="news")?"class='active'":""?>><a href="/my/news">Новости</a></li>
            <li <?php echo ($active=="articles")?"class='active'":""?>><a href="/my/articles">Статьи</a></li>
        </ul>
    </div>
</div>