<?php
/**
 * @author Pavel Evsegneev
 * Date: 25.01.13
 * Time: 13:38
 */
?>

<div class="row-fluid">
    <div class="span6 offset3">
        <table class="table table-condensed">
            <thead>
            <tr>
                <td style="width:75%">Новости <?php echo "(",$news->count()," шт.)";?></td>
                <td><a  href="/my/add/news"><i class="icon-plus-sign"></i> Добавить новость</a></td>
            </tr>
            </thead>
            <tbody>

            <tr class="info">
                <td>Название</td>
                <td>Дата</td>
            </tr>
            <?php foreach($news as $n) {?>
            <tr>
                <td>
                    <p><?php echo $n->name;?></p>
                    <p><a href="/my/edit/news/<?php echo $n->id?>">Изменить</a> | <a href="/my/delete/news/<?php echo $n->id;?>">Удалить</a></p>
                </td>
                <td><?php echo date("d.m.Y",$n->date);?></td>
            </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>