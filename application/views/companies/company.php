<?php
/**
 * @author Pavel Evsegneev
 * Date: 18.01.13
 * Time: 13:51
 */
$c = $company['company'];
$tnin = $company['trainings'];
$t = $company['trainers'];
$a = $company['articles'];
$n = $company['news'];
?>

<div class="row-fluid">
    <div class="span6 offset3">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a> <span class="divider">/</span></li>
                    <li><a href="/companies">Компании</a> <span class="divider">/</span></li>
                    <li class="active"><?php echo $c->name;?></li>
                </ul>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6 company-name">
                <h3><?php echo $c->name;?></h3>
            </div>
            <div class="span6">
                <img src="<?php echo $c->logo;?>" alt="" width="100">
            </div>
        </div>
        <hr>
        <div class="row-fluid">
            <div class="span6">
                <p>Телефон: <?php echo $c->phone;?></p>
                <p>E-mail: <?php echo $c->email;?></p>
                <p>Сайт: <a href="<?php echo $c->site;?>"><?php echo $c->site;?></a></p>
                <p>Адрес: <?php echo $c->address;?></p>
                <p>Контактное лицо: <?php echo $c->contact;?></p>
            </div>
            <div class="span6 contact-information">
                <p>О компании</p>
                <hr>
                <p><a href="#description">Кратко о компании</a><a name="description"></a></p>
                <?php if($c->services!="") {?><p><a href="#services">Услуги</a></p><?php }?>
                <?php if($c->clients!="") {?><p><a href="#clients">Клиенты</a></p><?php }?>
                <?php if($c->additional!="") {?><p><a href="#additional">Дополнительная информация</a></p><?php }?>
                <?php if($t->count()!=0) {?><p><a href="#trainers">Тренеры</a></p><?php }?>
                <?php if($tnin->count()!=0) {?><p><a href="#trainings">Тренинги</a></p><?php }?>
                <?php if($a->count()!=0) {?><p><a href="#articles">Статьи</a></p><?php }?>
                <?php if($n->count()!=0) {?><p><a href="#news">Новости</a></p><?php }?>

            </div>
        </div>
        <hr>

        <div class="row-fluid">
            <div class="span12">
                <h5>Кратко о компании</h5>
                <p><?php echo $c->description;?></p>
            </div>
        </div>

        <?php if($c->services!="") {?>
            <a name="services"></a>
            <div class="row-fluid">
                <div class="span12">

                    <h5>Услуги</h5>
                    <p><?php echo $c->services;?></p>
                </div>
            </div>
        <?php }?>

        <?php if($c->clients!="") {?>
        <a name="clients"></a>
        <div class="row-fluid">
            <div class="span12">
                <h5>Клиенты</h5>
                <p><?php echo $c->clients;?></p>
            </div>
        </div>
        <?php }?>

        <?php if($c->additional!="") {?>
        <a name="additional"></a>
        <div class="row-fluid">
            <div class="span12">
                <h5>Дополнительная информация</h5>
                <p><?php echo $c->additional;?></p>
            </div>
        </div>
        <?php }?>

        <?php if($t->count()!=0) {?>
        <a name="trainers"></a>
        <div class="row-fluid">
            <div class="span12">
                <h5>Тренеры</h5>
                <?php foreach($t as $trn) {?>
                    <div class="row-fluid">
                        <div class="span2">
                            <a href="/trainers/<?php echo $trn->id?>"><img src="<?php echo $trn->photo;?>" alt=""></a>
                        </div>
                        <div class="span10">
                            <p><a href="/trainers/<?php echo $trn->id?>"><?php echo $trn->name;?></a></p>
                            <p><?php echo trim(UTF8::substr(strip_tags($trn->description),0,300)),"&#133; ";?><a href="/trainers/<?php echo $trn->id?>">подробнее</a></p>
                        </div>
                    </div>
                <?php }?>
            </div>
        </div>
        <?php }?>

        <?php if($tnin->count()!=0) {?>
        <a name="trainings"></a>
        <div class="row-fluid">
            <div class="span12">
                <h5>Тренинги</h5>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Тренинг</th>
                        <th>Компания</th>
                        <th>Место, дата</th>
                        <th>Стоимость</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($tnin as $t) {
                            ?>
                        <tr>
                            <td>
                                <a href="<?php echo "/trainings/".$t->id;?>"><?php echo $t->name; ?></a>
                                <div>
                                    <?php echo $t->description;?>
                                </div>
                            </td>
                            <td><a><?php echo $t->company_name; ?></a></td >
                            <td >
                                <p><?php echo $t->city; ?> <?php echo $t->place; ?></p>
                                <p><?php echo date("d.m.Y",$t->date_start);?> &mdash; <?php echo date("d.m.Y",$t->date_end);?></p>
                            </td >
                            <td ><?php echo $t->price; ?> грн.</td >
                        </tr >

                            <?php
                        }
                        ?>
                    </tbody >
                </table>
            </div>
        </div>
        <?php }?>

        <?php if($a->count()!=0) {?>
            <a name="articles"></a>
            <div class="row-fluid">
                <h5>Статьи</h5>
                <div class="span12">
                    <ul>
                        <?php foreach($a as $ar) {?>
                            <li><a href="/articles/<?php echo $ar->id;?>"><?php echo $ar->name;?></a></li>
                        <?php }?>
                    </ul>
                </div>
            </div>
        <?php }?>

        <?php if($n->count()!=0) {?>
        <a name="news"></a>
        <div class="row-fluid">
            <h5>Новости</h5>
            <div class="span12">
                <ul>
                    <?php foreach($n as $nw) {?>
                    <li><?php echo date("d.m.Y",$nw->date);?> <a href="/news/<?php echo $nw->id;?>"><?php echo $nw->name;?></a></li>
                    <?php }?>
                </ul>
            </div>
        </div>
        <?php }?>

    </div>
</div>