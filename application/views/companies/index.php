<?php
/**
 * @author Pavel Evsegneev
 * Date: 18.01.13
 * Time: 13:51
 */
?>

<div class="row-fluid" xmlns="http://www.w3.org/1999/html">
    <div class="span6 offset3">

        <?php foreach($companies as $c) {?>
            <hr>
            <div class="row-fluid">

                <div class="span2">
                    <a href="/companies/<?php echo $c->id?>"><img src="<?php echo $c->logo;?>" alt=""></a>
                </div>

                <div class="span8">
                    <p><a href="/companies/<?php echo $c->id?>"><?php echo $c->name?></a></p>
                    <p><?php echo trim(Text::limit_chars(strip_tags($c->description),300));?><a href="/companies/<?php echo $c->id?>"> подробнее</a></p>
                </div>

                <div class="span2">
                    <?php if($c->trainers!=0) {?>
                        <p><a href="/companies/<?php echo $c->id?>#trainers">Тренеры: <?php echo $c->trainers?></a></p>
                    <?php }?>
                    <?php if($c->trainings!=0) {?>
                        <p><a href="/companies/<?php echo $c->id?>#trainings">Тренинги: <?php echo $c->trainings?></a></p>
                    <?php }?>
                    <?php if($c->articles!=0) {?>
                        <p><a href="/companies/<?php echo $c->id?>#articles">Статьи: <?php echo $c->articles?></a></p>
                    <?php }?>
                    <?php if($c->news!=0) {?>
                        <p><a href="/companies/<?php echo $c->id?>#news">Новости: <?php echo $c->news?></a></p>
                    <?php }?>
                </div>

            </div>
        <?php }?>

    </div>
</div>