<?php
/**
 * @author Pavel Evsegneev
 * Date: 21.01.13
 * Time: 14:59
 */
?>

<div class="row-fluid">
    <div class="span6 offset3">
        <form class="form-horizontal">
            <fieldset>
                <div id="legend" class="">
                    <legend class="">Company</legend>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="name">Название</label>
                    <div class="controls">
                        <input type="text" placeholder="Название компании" id='name' class="input-xlarge">
                        <p class="help-block">Пример: "ОАО Гребешок"</p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">Кратко о компании</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" class="" style="margin: 0px; width: 269px; height: 61px;"> </textarea>
                        </div>
                    </div>
                </div>



                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">Услуги</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" class="" style="margin: 0px; width: 269px; height: 69px;"> </textarea>
                        </div>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">Клиенты</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" class="" style="margin: 0px; width: 271px; height: 55px;"> </textarea>
                        </div>
                    </div>
                </div>





                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">Дополнительная информация</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" class="" style="margin: 0px; width: 271px; height: 52px;"> </textarea>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Логотип</label>

                    <!-- File Upload -->
                    <div class="controls">
                        <input class="input-file" id="fileInput" type="file">
                    </div>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="phone">Телефон</label>
                    <div class="controls">
                        <input type="text" placeholder="Введите номер телефона" id="phone" class="input-xlarge">
                        <p class="help-block">Пример: 223-322-223-322</p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="contact">Контактное лицо</label>
                    <div class="controls">
                        <input type="text" placeholder="Имя" id='contact' class="input-xlarge">
                        <p class="help-block">Пример: "Ирина", "Василий Пупкин"</p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="email">E-mail</label>
                    <div class="controls">
                        <input type="text" placeholder="Адрес электронной почты" id='email' class="input-xlarge">
                        <p class="help-block">Пример: address@example.com</p>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"></label>

                    <!-- Button -->
                    <div class="controls">
                        <button class="btn btn-default">Сохранить</button>
                    </div>
                </div>

            </fieldset>
        </form>

    </div>
</div>