<?php
/**
 * @author Pavel Evsegneev
 * Date: 21.01.13
 * Time: 12:58
 */
$n = $news_item;
?>
<div class="row-fluid">
    <div class="span6 offset3">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a> <span class="divider">/</span></li>
                    <li><a href="/news">Новости</a> <span class="divider">/</span></li>
                    <li class="active"><?php echo $n->name;?></li>
                </ul>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12 article-name">
                <h3><?php echo $n->name;?></h3>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <p><?php echo date("d.m.Y", $n->date);?></p>
                <p><a href="/companies/<?php echo $n->company_id;?>"><?php echo $n->company_name;?></a></p>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <strong><?php echo $n->introduction;?></strong>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <?php echo $n->text;?>
                <hr>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <p>Компания: <a href="/companies/<?php echo $n->company_id;?>"><?php echo $n->company_name;?></a></p>
            </div>
        </div>
    </div>
</div>