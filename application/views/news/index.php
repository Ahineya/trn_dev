<?php
/**
 * @author Pavel Evsegneev
 * Date: 21.01.13
 * Time: 12:58
 */
?>
<div class="row-fluid">
    <div class="span6 offset3">

        <?php foreach($news as $n) {?>
        <div class="row-fluid">
            <hr>
            <div class="span2">
                <p>
                    <?php echo date("d.m.Y", $n->date);?>
                </p>
            </div>
            <div class="span10">
                <p><a href="/news/<?php echo $n->id;?>"><?php echo $n->name;?></a></p>
                <p><a href="/companies/<?php echo $n->company_id;?>"><?php echo $n->company_name;?></a></p>
                <p><?php echo $n->introduction;?></p>
            </div>
        </div>
        <?php }?>

    </div>
</div>