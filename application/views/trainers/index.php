<?php
/**
 * @author Pavel Evsegneev
 * Date: 18.01.13
 * Time: 11:38
 */



foreach($trainers as $t) {
?>

    <div class="row-fluid">
        <div class="span1 offset2">
            <a href="/trainers/<?php echo $t->id?>"><img src="<?php echo $t->photo;?>" alt=""></a>
        </div>
        <div class="span7">
            <p><a href="/trainers/<?php echo $t->id?>"><?php echo $t->name;?></a></p>
            <p><a href="/companies/<?php echo $t->company_id;?>"><?php echo $t->company_name;?></a></p>
            <p><?php echo trim(UTF8::substr(strip_tags($t->description),0,300)),"&#133; ";?><a href="/trainers/<?php echo $t->id?>">подробнее</a></p>
        </div>
    </div>

<?php }?>