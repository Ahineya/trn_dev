<?php
/**
 * @author Pavel Evsegneev
 * Date: 18.01.13
 * Time: 11:38
 */
$t = $trainer['trainer'];
?>

<div class="row-fluid">
    <div class="span6 offset3">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a> <span class="divider">/</span></li>
                    <li><a href="/trainers">Тренеры</a> <span class="divider">/</span></li>
                    <li class="active"><?php echo $t->name;?></li>
                </ul>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12 trainer-name">
                <h3><?php echo $t->name;?></h3>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span2">
                <img src="<?php echo $t->photo;?>" alt="">
            </div>
            <div class="span5">
                <p><?php echo ($t->phone!="")?"Телефон: ".$t->phone:"";?></p>
                <p><?php echo ($t->email!="")?"E-mail: ".$t->email:"";?></p>
                <p><?php echo ($t->site!="")?"Сайт: <a href='".$t->site."'>".$t->site."</a>":"";?></p>
                <p>Компания: <a href="/companies/<?php echo $t->company_id;?>"><?php echo $t->company_name;?></a></p>
            </div>
            <div class="span5 contact-information">
                <p>О тренере<hr></p>
                <p><a href=#info>Подробная информация</a><a name="info"></a></p>
                <?php echo ($trainer['trainings']->count()!=0)?"<p><a href='#trainings'>Тренинги</a></p>":"";?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <h5>Подробная информация</h5>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <?php echo $t->description?><a name="trainings"></a>
            </div>
        </div>
        <?php if ($trainer['trainings']->count()!=0) {
        ?>
            <div class="row-fluid">
                <div class="span12">
                    <p><h5>Тренинги</h5></p>
                </div>
            </div>
            <?php foreach($trainer['trainings'] as $tr) {
            ?>
                <div class="row-fluid">
                    <div class="span12">
                        <a href="/trainings/<?php echo $tr->id ?>"><?php echo $tr->name ?></a>
                    </div>
                </div>
            <?php }?>

        <?php }?>
    </div>
</div>