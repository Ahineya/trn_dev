<?php
/**
 * @author Pavel Evsegneev
 * Date: 21.01.13
 * Time: 14:59
 */
?>

<div class="row-fluid">
    <div class="span6 offset3">
        <form class="form-horizontal" method="post" action="/registration/register">
            <fieldset>
                <div id="legends">
                    <legend class="">Пользователь</legend>
                </div>

                        <div class="control-group">

                            <!-- Text input-->
                            <label class="control-label" for="email">Почта</label>
                            <div class="controls">
                                <input type="text" placeholder="E-mail" id="email" name="email" class="input-xlarge" required>
                                <p class="help-block"></p>
                            </div>
                        </div>

                        <div class="control-group">

                            <!-- Text input-->
                            <label class="control-label" for="password">Пароль</label>
                            <div class="controls">
                                <input type="password" placeholder="Пароль" id="password" name="password" class="input-xlarge" required>
                                <p class="help-block"></p>
                            </div>
                        </div>

                        <div class="control-group">

                            <!-- Text input-->
                            <label class="control-label" for="password_confirm">Повтор пароля</label>
                            <div class="controls">
                                <input type="password" placeholder="Повтор пароля" id="password_confirm" name ="password_confirm" class="input-xlarge" required>
                                <p class="help-block"></p>
                            </div>
                        </div>

            </fieldset>
            <fieldset>
                <div id="legend" class="">
                    <legend class="">Компания</legend>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="name">Название</label>
                    <div class="controls">
                        <input type="text" placeholder="Название компании" id='name' name="name" class="input-xlarge" required>
                        <p class="help-block">Пример: "ОАО Гребешок"</p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Textarea -->
                    <label class="control-label">Кратко о компании</label>
                    <div class="controls">
                        <div class="textarea">
                            <textarea type="" style="margin: 0px; width: 269px; height: 61px;" class='editor' name="description" required> </textarea>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <!-- Select Basic -->
                    <label class="control-label">Город</label>
                    <div class="controls">
                        <select class="input-xlarge" name='city' required>
                            <?php foreach($cities as $c) { ?>
                                <option value="<?php echo $c['id'];?>"><?php echo $c['name'];?></option>
                            <?php }?>

                        </select>
                    </div>
                </div>


                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="address">Адрес (без указания города)</label>
                    <div class="controls">
                        <input type="text" placeholder="Адрес" id='address' name="address" class="input-xlarge" required>
                        <p class="help-block">Пример: "Ул.Бальзака, 60"</p>
                    </div>
                </div>


                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="phone">Телефон</label>
                    <div class="controls">
                        <input type="text" placeholder="Введите номер телефона" id="phone" class="input-xlarge" name="phone">
                        <p class="help-block">Пример: 223-322-223-322</p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="site">Сайт</label>
                    <div class="controls">
                        <input type="text" placeholder="http://" id='site' name="site" class="input-xlarge">
                        <p class="help-block">Пример: "ОАО Гребешок"</p>
                    </div>
                </div>

                <div class="control-group">

                    <!-- Text input-->
                    <label class="control-label" for="contact">Контактное лицо</label>
                    <div class="controls">
                        <input type="text" placeholder="Имя" id='contact' class="input-xlarge" name="contact" required>
                        <p class="help-block">Пример: "Ирина", "Василий Пупкин"</p>
                    </div>
                </div>


                <div class="control-group">
                    <label class="control-label"></label>

                    <!-- Button -->
                    <div class="controls">
                        <button class="btn btn-default">Сохранить</button>
                    </div>
                </div>

            </fieldset>
        </form>

    </div>
</div>