<?php
/**
 * @author Pavel Evsegneev
 * Date: 22.01.13
 * Time: 12:54
 */
?>

<div class="row-fluid">
    <div class="span6 offset3">
        <div class="row-fluid">
            <div class="span6">

                <form class="form-horizontal" method="post" action="/login/login">
                    <fieldset>
                        <div class="">
                            <legend class="">Вход</legend>
                        </div>
                        <div class="control-group">

                            <!-- Text input-->
                            <label class="control-label" for="email">Почта</label>
                            <div class="controls">
                                <input type="text" placeholder="E-mail" id='email' name='email' class="input-xlarge">
                                <p class="help-block"></p>
                            </div>
                        </div>

                        <div class="control-group">

                            <!-- Text input-->
                            <label class="control-label" for="password">Пароль</label>
                            <div class="controls">
                                <input type="password" placeholder="Пароль" id="password" name="password" class="input-xlarge">
                                <p class="help-block"></p>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"></label>

                            <!-- Button -->
                            <div class="controls">
                                <button class="btn btn-default">Войти</button>
                            </div>
                        </div>

                    </fieldset>
                </form>

            </div>
            <div class="span5 offset1">
                <form class="form-horizontal">
                    <fieldset>
                        <div class="">
                            <legend class="">Регистрация</legend>
                        </div>
                    </fieldset>
                </form>
                Потратив пару минут для регистрации, вы сможете:
                <ul>
                    <li>размещать и редактировать свои тренинги;</li>
                    <li>добавить информацию о вашей компании и тренерах;</li>
                    <li>публиковать новости компании и статьи тренеров.</li>
                </ul>
                <a href="/registration/" class='btn'>Зарегистрироваться</a>
            </div>
        </div>
    </div>
</div>