<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="<?php echo $meta['keywords'] ?>">
    <meta name="description" content="<?php echo $meta['description'] ?>">

    <link rel="shortcut icon" href="<?php echo URL::site('favicon.png') ?>">

    <!-- Le styles -->
    <?php foreach ($styles as $style => $media): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $style ?>"
          media="<?php echo $media ?>"/>
    <?php endforeach; ?>
    <style type="text/css">
        body {
            padding-top: 60px;
            padding-bottom: 40px;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse"
               data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="/">Trn</a>

            <div class="nav-collapse">
                <ul class="nav">
                    <li <?php if ($controller =="main") echo 'class="active"'?>><a href="/">Home</a></li>
                    <li <?php if ($controller =="trainings") echo 'class="active"'?>><a href="/trainings">Тренинги</a></li>
                    <li <?php if ($controller =="companies") echo 'class="active"'?>><a href="/companies">Компании</a></li>
                    <li <?php if ($controller =="trainers") echo 'class="active"'?>><a href="/trainers">Тренеры</a></li>
                    <li <?php if ($controller =="articles") echo 'class="active"'?>><a href="/articles">Статьи</a></li>
                    <li <?php if ($controller =="news") echo 'class="active"'?>><a href="/news">Новости</a></li>
                </ul>
                <ul class="nav pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php $cm = Session::instance()->get('company_name'); if(!empty($cm)) {echo $cm;} else echo "login" ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <li>
                                <a href="/login">Вход</a>
                            </li>
                            <li>
                                <a href="/my">Моя страница</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="/logout">Выход</a>
                            </li>

                        </ul>
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <?php echo Message::display(); //display flash_message ?>
            <?php if($message!=""){
            $m =<<<html
            <div class="row-fluid">
                <div class="span6 offset3">
                    <div class="alert alert-$message_type">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      $message
                    </div>
                </div>
            </div>
html;
            echo $m;
            }?>
            <?php if($controller=='my') echo $controls ?>
            <?php echo $content ?>
        </div>
    </div>
    <hr>

    <footer class='footer_main'>
        <p>&copy; Sait.ua 2012</p>
    </footer>

</div>
<!--/.fluid-container-->

<!-- Load Javascripts -->
<script type="text/javascript">var BASEURL = "<?php echo URL::base() ?>";</script>
<?php foreach ($scripts as $script): ?>
<script type="text/javascript" src="<?php echo $script ?>"></script>
    <?php endforeach; ?>

</body>
</html>
