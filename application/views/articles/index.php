<?php
/**
 * @author Pavel Evsegneev
 * Date: 18.01.13
 * Time: 15:38
 */
?>

<div class="row-fluid">
    <div class="span6 offset3">

        <?php foreach($articles as $a) {?>
            <div class="row-fluid">
                <div class="span12">
                    <hr>
                    <p><a href="/articles/<?php echo $a->id;?>"><?php echo $a->name;?></a></p>
                    <p><a href="/companies/<?php echo $a->company_id;?>"><?php echo $a->company_name;?></a> | <?php echo($a->trainer_id!="")?"<a href='/trainers/".$a->trainer_id."'>".$a->trainer_name."</a>":$a->author;?></p>
                    <p><?php echo $a->introduction;?></p>
                </div>
            </div>
        <?php }?>

    </div>
</div>