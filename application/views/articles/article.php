<?php
/**
 * @author Pavel Evsegneev
 * Date: 18.01.13
 * Time: 15:38
 */
$a=$article;
?>

<div class="row-fluid">
    <div class="span6 offset3">
        <div class="row-fluid">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a> <span class="divider">/</span></li>
                    <li><a href="/articles">Статьи</a> <span class="divider">/</span></li>
                    <li class="active"><?php echo $a->name;?></li>
                </ul>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12 article-name">
                <h3><?php echo $a->name;?></h3>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <p><a href="/companies/<?php echo $a->company_id;?>"><?php echo $a->company_name;?></a> | <?php echo($a->trainer_id!="")?"<a href='/trainers/".$a->trainer_id."'>".$a->trainer_name."</a>":$a->author;?></p>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <strong><?php echo $a->introduction;?></strong>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <?php echo $a->text;?>
                <hr>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <p>Автор: <?php echo($a->trainer_id!="")?"<a href='/trainers/".$a->trainer_id."'>".$a->trainer_name."</a>":$a->author;?></p>
                <p>Компания: <a href="/companies/<?php echo $a->company_id;?>"><?php echo $a->company_name;?></a></p>
            </div>
        </div>
    </div>
</div>