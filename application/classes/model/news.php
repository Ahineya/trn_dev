<?php
/**
 * @author Pavel Evsegneev
 * Date: 21.01.13
 * Time: 12:58
 */
class Model_News extends Model_Base
{

    //-------- Get functions --------------

    public function getNews() {
        $q=<<<SQL
SELECT n.id, n.name, n.introduction, n.active, UNIX_TIMESTAMP(n.date) date, c.id company_id, c.name company_name FROM news n
JOIN company c ON c.id = n.company_id
WHERE n.active = 1
SQL;
        $result = $this->_db->query(Database::SELECT,$q,true);
        return $result;
    }

    public function getNewsItem($id) {
        $q=<<<SQL
SELECT n.id, n.name, n.introduction, n.text, UNIX_TIMESTAMP(n.date) date, n.active, n.visible, n.author, n.trainer_id, c.id company_id, c.name company_name FROM news n
JOIN company c ON c.id = n.company_id
WHERE n.id = $id;
SQL;
        $result = $this->_db->query(Database::SELECT,$q,true);
        $result = $result[0];
        try {
            if($result->active==1) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function getNewsByCompany($id) {
        $q=<<<SQL
SELECT *, UNIX_TIMESTAMP(n.date) date FROM news n
WHERE n.company_id=$id
SQL;
        $result = $this->_db->query(Database::SELECT,$q,true);
        return $result;

    }

    //-------- Insert functions --------------

    public function addNews($news) {
        $news = array_map('trim', $news);

        /**
         * @var $caption
         * @var $introduction
         * @var $text
         * @var $trainer
         * @var $author
         * @var $select_author
         */

        extract($news);
        $caption = $this->_db->escape($caption);
        $introduction = $this->_db->escape($introduction);
        $text = $this->_db->escape($text);

        $what_author = $select_author==1?"`trainer_id` ":"`author` ";
        $author = $select_author==1?$trainer:$this->_db->escape($author);

        $cid = Session::instance()->get('cid');
        $q=<<<SQL
INSERT INTO news
(`company_id`,
`date`,
`introduction`,
`name`,
`text`,
$what_author)
VALUES
(
$cid,
NOW(),
$introduction,
$caption,
$text,
$author
);

SQL;
        $this->_db->query(Database::INSERT,$q);
    }

    //-------- Update functions --------------

    public function editNews($nid, $news) {

        /**
         * @var $caption
         * @var $introduction
         * @var $text
         * @var $trainer
         * @var $author
         * @var $select_author
         */

        extract($news);
        $caption = $this->_db->escape($caption);
        $introduction = $this->_db->escape($introduction);
        $text = $this->_db->escape($text);

        $what_author = $select_author==1?"`trainer_id` ":"`author` ";
        $author = $select_author==1?$trainer:$this->_db->escape($author);

        $q =<<<SQL
UPDATE news
SET `author` = NULL,
`trainer_id` = NULL
WHERE id = $nid
SQL;

        $this->_db->query(Database::UPDATE, $q);

        unset($q);

        $q=<<<SQL
UPDATE news
SET
`introduction` = $introduction,
`name` = $caption,
`text` = $text,
`visible` = 0,
$what_author = $author
WHERE id = $nid;

SQL;

        $this->_db->query(Database::UPDATE, $q);

    }

    //-------- Delete functions --------------

    public function deleteNews($nid) {
        $cid=Session::instance()->get('cid');
        $q=<<<SQL
UPDATE news
SET `active`='0' WHERE `id`=$nid;

SQL;
        if($this->isCompanyNews($nid)) {
            $this->_db->query(Database::DELETE, $q);
            return true;
        } else {
            return false;
        }

    }

    //-------- Mix functions --------------
    public function isCompanyNews($nid) {
        $cid=Session::instance()->get('cid');
        $q = "SELECT * FROM news WHERE id=$nid";
        $result = $this->_db->query(Database::SELECT, $q, true);
        $result = $result[0];
        return ($result->company_id==$cid);

    }

}
