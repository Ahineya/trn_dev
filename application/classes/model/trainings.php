<?php
/**
 * @author Pavel Evsegneev
 * Date: 17.01.13
 * Time: 14:17
 */
class Model_trainings extends Model_Base
{
    public function getTrainings() {
        $q =<<<SQL
SELECT t.id, t.`name`, t.description, ct.name city, p.address place, t.price,
UNIX_TIMESTAMP(t.date_start) date_start, UNIX_TIMESTAMP(t.date_end) date_end,
c.`name` company_name, c.id company_id
FROM training t
JOIN company c on c.id = t.company_id
LEFT JOIN place p ON t.place = p.id
LEFT JOIN city ct ON ct.id = p.city
WHERE t.active = 1
SQL;

        $result = $this->_db->query(DATABASE::SELECT, $q, true);
        return $result;
    }

    public function getTraining($id) {
        $q =<<<SQL
SELECT t.`name`, t.audience, t.goal, t.description, t.additional, ct.name city, p.address place, t.price, t.document, t.active,
UNIX_TIMESTAMP(p.date_start) date_start, UNIX_TIMESTAMP(p.date_end) date_end, t.visible, t.type, t.form, t.price_comment,
t.email, t.duration, p.time_start,
c.`name` AS company_name, c.phone, c.contact, c.id company_id
FROM training t
JOIN company c on c.id = t.company_id
LEFT JOIN place p ON p.id = t.place
LEFT JOIN city ct ON ct.id = p.city
WHERE t.id = $id;
SQL;

        $training = $this->_db->query(DATABASE::SELECT, $q, true);
        $result['training'] = $training[0];
        try {
            if($result['training']->active==1) {
                $result['trainers'] = $this->getTrainingTrainers($id);
                return $result;
            } else return false;
        } catch(Exception $e) {
            return false;
        }


        return $result;
    }

    public function getTrainingTrainers($id) {
        $q =<<<SQL
SELECT *,t.id trainer_id FROM trainer t
JOIN trainer_to_training ttt ON t.id = ttt.trainer_id
WHERE ttt.training_id = $id
SQL;
        $result = $this->_db->query(DATABASE::SELECT, $q, true);
        return $result;
    }

    public function getTrainingsByTrainer($id) {
        $q =<<<SQL
SELECT * FROM trainer_to_training ttt
JOIN training t ON t.id = ttt.training_id
WHERE ttt.trainer_id=$id
SQL;
        $result = $this->_db->query(Database::SELECT,$q,true);
        return $result;
    }

    public function getTrainingsByCompany($id) {
        $q =<<<SQL
SELECT t.id, t.`name`, t.audience, t.goal, t.description, t.additional, ct.name city, p.address place, t.price, t.document,
t.trainer_id, tr.name trainer_name, t.active,
UNIX_TIMESTAMP(t.date_start) date_start, UNIX_TIMESTAMP(t.date_end) date_end,
c.`name` AS company_name, c.phone, c.contact
FROM training t
JOIN company c on c.id = t.company_id
LEFT JOIN trainer tr ON t.trainer_id=tr.id
LEFT JOIN place p ON p.id = t.place
LEFT JOIN city ct ON ct.id = p.city
WHERE c.id = $id AND t.active = 1;
SQL;
        $result = $this->_db->query(Database::SELECT,$q,true);
        return $result;
    }

    public function getTrainingCategories($tid) {
        $q=<<<SQL
SELECT c.name FROM category_to_training ctt
LEFT JOIN category c ON ctt.category_id = c.id
WHERE ctt.training_id = $tid
SQL;
        return $this->_db->query(Database::SELECT, $q, true);
    }

    //--------- Add functions ----------------

    public function addTraining($post, $id=0, $step=1) {
        switch($step) {
            case 1: {
                //echo Debug::vars($post);

                /**
                 * @var $name
                 * @var $trainers
                 * @var $categories
                 * @var $form
                 * @var $type
                 * @var $price
                 * @var $price_comment
                 * @var $contact
                 * @var $phone
                 * @var $email
                 */

                extract($post);
                $price = (isset($is_free))?0:$price;

                $cid = Session::instance()->get('cid');

                $name = $this->_db->escape($name);
                $form = $this->_db->escape($form);
                $price = $this->_db->escape($price);
                $price_comment = $this->_db->escape($price_comment);
                $type = $this->_db->escape($type);
                $phone = $this->_db->escape($phone);
                $email = $this->_db->escape($email);
                $contact = $this->_db->escape($contact);

                $q=<<<SQL
INSERT INTO `trn_dev`.`training`
(
`company_id`,
`form`,
`name`,
`price`,
`price_comment`,
`type`,
`phone`,
`email`,
`contact`
)
VALUES
(
$cid,
$form,
$name,
$price,
$price_comment,
$type,
$phone,
$email,
$contact
);
SQL;

                $result = $this->_db->query(Database::INSERT, $q);
                $last_id = $result[0];

                foreach($trainers as $t) {
                    $q="INSERT INTO trainer_to_training (`trainer_id`, `training_id`) VALUES ($t, $last_id);";
                    $this->_db->query(Database::INSERT, $q);
                }
                foreach($categories as $c) {
                    $q="INSERT INTO category_to_training (`category_id`, `training_id`) VALUES ($c, $last_id);";
                    $this->_db->query(Database::INSERT, $q);
                }

                return $last_id;

                break;
            }
            case 2: {

                /**
                 * @var $duration
                 * @var $document
                 * @var $audience
                 * @var $goal
                 * @var $description
                 * @var $additional
                 */

                extract($post);

                $last_id = Session::instance()->get('last_id');

                $duration = $this->_db->escape($duration);
                $document = $this->_db->escape($document);
                $audience = $this->_db->escape($audience);
                $goal = $this->_db->escape($goal);
                $description = $this->_db->escape($description);
                $additional = $this->_db->escape($additional);

                $q=<<<SQL
UPDATE training
SET
`additional` = $additional,
`audience` = $audience,
`description` = $description,
`document` = $document,
`duration` = $duration,
`goal` = $goal
WHERE id = $last_id;

SQL;

                $this->_db->query(Database::UPDATE, $q);

                break;
            }
            case 3: {
                /**
                 * @var $date_start
                 * @var $date_end
                 * @var $time_start
                 * @var $city
                 * @var $address
                 */

                extract($post);

                $last_id = Session::instance()->get('last_id');

                $date_start = Func::date2sql($date_start);
                $date_end = Func::date2sql($date_end);

                $time_start = $this->_db->escape($time_start);
                $date_start = $this->_db->escape($date_start);
                $date_end = $this->_db->escape($date_start);
                $city = $this->_db->escape($time_start);
                $address = $this->_db->escape($address);

                $q=<<<SQL
UPDATE training
SET
`address` = $address,
`city` = $city,
`date_end` = $date_end,
`date_start` = $date_start,
`time_start` = $time_start
WHERE id = $last_id;

SQL;

                $this->_db->query(Database::UPDATE, $q);    //Deprecated, need to use place table

                unset($q);

                $q=<<<SQL
INSERT INTO place
(`address`,
`city`,
`date_end`,
`date_start`,
`time_start`)
VALUES
(
$address,
$city,
$date_end,
$date_start,
$time_start
);
SQL;
                $this->_db->query(Database::INSERT, $q);
            }
        }
    }

    //----------- Delete functions --------------
    public function deleteTraining($tid) {
        $cid=Session::instance()->get('cid');
        $q=<<<SQL
UPDATE training
SET `active`='0' WHERE `id`=$tid;
SQL;
        if($this->isCompanyTraining($tid)) {
            $this->_db->query(Database::UPDATE, $q);
            return true;
        } else {
            return false;
        }

    }

    //-------- Mix functions --------------
    public function isCompanyTraining($tid) {
        $cid=Session::instance()->get('cid');
        $q = "SELECT * FROM training WHERE id=$tid";
        $result = $this->_db->query(Database::SELECT, $q, true);
        $result = $result[0];
        return ($result->company_id==$cid);

    }

}