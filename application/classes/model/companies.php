<?php
/**
 * @author Pavel Evsegneev
 * Date: 18.01.13
 * Time: 13:52
 */
class Model_Companies extends Model_Base
{

    //------ Get functions ----------

    public function getCompanies() {
        $q=<<<SQL
SELECT *, c.id, c.name, c.description FROM company c
LEFT JOIN
    (SELECT cm.id cid, count(t.id) trainers
    FROM company cm
    LEFT JOIN trainer t ON cm.id=t.company_id
    WHERE t.active = 1
    GROUP BY cid) t
ON t.cid=c.id
LEFT JOIN
    (SELECT cm.id cid, count(tr.id) trainings
    FROM company cm
    LEFT JOIN training tr ON cm.id=tr.company_id
    WHERE tr.active = 1
    GROUP BY cid) tr
ON tr.cid=c.id
LEFT JOIN
    (SELECT cm.id cid, count(ar.id) articles
    FROM company cm
    LEFT JOIN article ar ON cm.id=ar.company_id
    WHERE ar.active = 1
    GROUP BY cid) a
ON a.cid=c.id
LEFT JOIN
    (SELECT cm.id cid, count(n.id) news
    FROM company cm
    LEFT JOIN news n ON cm.id=n.company_id
    WHERE n.active = 1
    GROUP BY cid) n
ON n.cid=c.id

GROUP BY c.id

SQL;
        $result = $this->_db->query(Database::SELECT,$q,true);
        return $result;
    }

    public function getCompany($id) {
        $q=<<<SQL
SELECT * FROM company c WHERE c.id=$id;
SQL;
        $company = $this->_db->query(Database::SELECT,$q,true);
        $trainers = Model::factory('trainers')->getTrainersByCompany($id);
        $trainings = Model::factory('trainings')->getTrainingsByCompany($id);
        $articles = Model::factory('articles')->getArticlesByCompany($id);
        $news = Model::factory('news')->getNewsByCompany($id);
        $result['company'] = $company[0];
        $result['trainers'] = $trainers;
        $result['trainings'] = $trainings;
        $result['articles'] = $articles;
        $result['news'] = $news;
        return $result;
    }

    //------ Set functions ----------

    public function createCompany() {
        if ( !preg_match('/^http:\/\//i', $_POST['site']) ) {
            $_POST['site'] = "http://".$_POST['site'];
        }
        $v = Validation::factory($_POST);
        $v->rule('email', 'not_empty')
            ->rule('email', 'email')
            ->rule('password', 'not_empty')
            ->rule('password_confirm', 'matches', array(':validation', ':field', 'password'))
            ->rule('description','not_empty')
            ->rule('address','not_empty')
            ->rule('contact','not_empty')
            ->rule('name','not_empty')
            ->rule('site','url');


        if($v->check()) {

            // Создаем переменную, отвечающую за связь с моделью данных User
            $model = ORM::factory('user');
            // Вносим в эту переменную значения, переданные из POST
            $model->values(array(
                'username' => $_POST['email'],
                'email' => $_POST['email'],
                'password' => $_POST['password'],
                'password_confirm' => $_POST['password_confirm'],
            ));
            try
            {
                // Пытаемся сохранить пользователя (то есть, добавить в
                $model->save();
                // Назначаем ему роли
                $model->add('roles', ORM::factory('role')->where('name', '=', 'login')->find());
                // И отправляем его на страницу пользователя
                //$this->request->redirect('/');
                $user_id = $model->id;
                extract($_POST);

                $address = $this->_db->escape($address);
                $city = $this->_db->escape($city);
                $contact = $this->_db->escape($contact);
                $description = $this->_db->escape($description);
                $email = $this->_db->escape($email);
                $name = $this->_db->escape($name);
                $phone = $this->_db->escape($phone);
                $site = $this->_db->escape($site);

                $q=<<<SQL
INSERT INTO company
(
`address`,
`city`,
`contact`,
`description`,
`email`,
`name`,
`phone`,
`site`,
`user_id`)
VALUES
(
"$address",
"$city",
"$contact",
"$description",
"$email",
"$name",
"$phone",
"$site",
$user_id
);

SQL;

                $this->_db->query(Database::INSERT, $q);

                return true;
            }
            catch (ORM_Validation_Exception $e)
            {
                // Это если возникли какие-то ошибки
                echo $e;
                return false;
            }
        } else {
            return false;
        }
    }


    public function saveCompany() {
        $v = Validation::factory($_POST);

    }

}
