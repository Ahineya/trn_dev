<?php
/**
 * @author Pavel Evsegneev
 * Date: 18.01.13
 * Time: 15:39
 */
class Model_Articles extends Model_Base
{
    public function getArticles() {
        $q=<<<SQL
SELECT *, a.id, a.name, t.id trainer_id, t.name trainer_name, c.id company_id, c.name company_name FROM article a
JOIN company c ON a.company_id=c.id
LEFT JOIN trainer t ON a.trainer_id=t.id
WHERE a.active=1;
SQL;
        $result = $this->_db->query(Database::SELECT,$q,true);
        return $result;
    }

    public function getArticle($id) {
        $q=<<<SQL
SELECT *, a.id, a.name, a.active, t.id trainer_id, t.name trainer_name, c.id company_id, c.name company_name FROM article a
JOIN company c ON a.company_id=c.id
LEFT JOIN trainer t ON a.trainer_id=t.id
WHERE a.id=$id;
SQL;
        $result = $this->_db->query(Database::SELECT,$q,true);
        $result = $result[0];
        try {
            if($result->active==1) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function getArticlesByCompany($id) {
        $q=<<<SQL
SELECT *, a.id, a.name, t.id trainer_id, t.name trainer_name, c.id company_id, c.name company_name, UNIX_TIMESTAMP(a.date) date FROM article a
JOIN company c ON a.company_id=c.id
LEFT JOIN trainer t ON a.trainer_id=t.id
WHERE c.id=$id AND a.active=1;
SQL;
        $result = $this->_db->query(Database::SELECT,$q,true);
        return $result;
    }

    public function getArticleCategories($aid) {
        $q=<<<SQL
SELECT * FROM category_to_article
WHERE article_id = $aid
SQL;
        return $this->_db->query(Database::SELECT, $q, true);
    }

    //-------- Insert functions --------------

    public function addArticle($article) {
        //$news = array_map('trim', $article);

        /**
         * @var $caption
         * @var $introduction
         * @var $text
         * @var $categories
         * @var $trainer
         * @var $author
         * @var $select_author
         */
        extract($article);
        $caption = $this->_db->escape($caption);
        $introduction = $this->_db->escape($introduction);
        $text = $this->_db->escape($text);
        //$categories
        $what_author = $select_author==1?"`trainer_id` ":"`author` ";
        $author = $select_author==1?$trainer:$this->_db->escape($author);

        $cid = Session::instance()->get('cid');
        $q=<<<SQL
INSERT INTO article
(`company_id`,
`date`,
`introduction`,
`name`,
`text`,
$what_author
)
VALUES
(
$cid,
NOW(),
$introduction,
$caption,
$text,
$author
);

SQL;
        $result = $this->_db->query(Database::INSERT,$q);
        $last_id = $result[0];

        foreach($categories as $c) {
            unset($q);
            $q =<<<SQL
INSERT INTO category_to_article
(`article_id`,
`category_id`
)
VALUES
(
$last_id,
$c
);

SQL;
            $this->_db->query(Database::INSERT,$q);
        }

    }

    //-------- Update functions --------------

    public function editArticle($aid, $article) {
        /**
         * @var $caption
         * @var $introduction
         * @var $text
         * @var $categories
         * @var $trainer
         * @var $author
         * @var $select_author
         */
        extract($article);
        $caption = $this->_db->escape($caption);
        $introduction = $this->_db->escape($introduction);
        $text = $this->_db->escape($text);
        $what_author = $select_author==1?"`trainer_id` ":"`author` ";
        $author = $select_author==1?$trainer:$this->_db->escape($author);

        $q =<<<SQL
UPDATE article
SET `author` = NULL,
`trainer_id` = NULL
WHERE id = $aid;
SQL;

        $this->_db->query(Database::UPDATE, $q);

        unset($q);

        $q =<<<SQL
UPDATE article
SET
$what_author = $author,
`introduction` = $introduction,
`name` = $caption,
`text` = $text,
`visible` = 0
WHERE id=$aid;

SQL;
        $this->_db->query(Database::UPDATE, $q);

        unset($q);

        $q=<<<SQL
DELETE FROM category_to_article
WHERE article_id = $aid;
SQL;

        $this->_db->query(Database::DELETE, $q);

        foreach($categories as $c) {
            unset($q);
            $q =<<<SQL
INSERT INTO category_to_article
(`article_id`,
`category_id`
)
VALUES
(
$aid,
$c
);

SQL;
            $this->_db->query(Database::INSERT,$q);
        }

    }


    //-------- Delete functions --------------

    public function deleteArticle($aid) {
        //$cid=Session::instance()->get('cid');
        $q=<<<SQL
UPDATE article
SET `active`='0' WHERE `id`=$aid;
SQL;
        if($this->isCompanyArticle($aid)) {
            $this->_db->query(Database::DELETE, $q);
            return true;
        } else {
            return false;
        }

    }

    //-------- Mix functions --------------
    public function isCompanyArticle($aid) {
        $cid=Session::instance()->get('cid');
        $q = "SELECT * FROM article WHERE id=$aid";
        $result = $this->_db->query(Database::SELECT, $q, true);
        $result = $result[0];
        return ($result->company_id==$cid);

    }

}
