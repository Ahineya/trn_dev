<?php
/**
 * @author Pavel Evsegneev
 * Date: 23.01.13
 * Time: 17:23
 */
class Model_Login extends Model_Base
{
    public function getUserCompany($uid) {
        $q=<<<SQL
SELECT id, name FROM company WHERE user_id=$uid
SQL;
        $result = $this->_db->query(Database::SELECT, $q, true);
        return $result[0];
    }
}
