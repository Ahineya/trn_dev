<?php
/**
 * @author Pavel Evsegneev
 * Date: 18.01.13
 * Time: 11:38
 */
class Model_Trainers extends Model_Base
{
    public function getTrainers() {
        $q =<<<SQL
SELECT t.id, t.photo, t.name, t.description, t.active, c.id company_id, c.name company_name
FROM trainer t
JOIN company c ON t.company_id = c.id
WHERE t.active=1;
SQL;
        $result = $this->_db->query(Database::SELECT,$q, true);
        return $result;
    }

    public function getTrainer($id) {
        $q =<<<SQL
SELECT t.id, t.photo, t.name, t.description, t.email, t.phone, t.site, t.active, c.id company_id, c.name company_name
FROM trainer t
JOIN company c ON t.company_id = c.id
WHERE t.id = $id;
SQL;
        $trainer = $this->_db->query(Database::SELECT,$q, true);
        $result['trainer'] = $trainer[0];
        try {
            if($result['trainer']->active==1) {
                $result['trainings'] = Model::factory('trainings')->getTrainingsByTrainer($id);
                return $result;
            } else return false;
        } catch(Exception $e) {
            return false;
        }
    }

    public function getTrainersByCompany($id) {
        $q=<<<SQL
SELECT * FROM trainer t WHERE company_id=$id AND t.active=1
SQL;
        $result = $this->_db->query(Database::SELECT,$q,true);
        return $result;
    }

    //-------- Insert functions --------------

    public function addTrainer($trainer) {
        //$news = array_map('trim', $article);

        /**
         * @var $name
         * @var $description
         * @var $phone
         * @var $email
         * @var $site
         */
        extract($trainer);
        $name = $this->_db->escape($name);
        $description = $this->_db->escape($description);
        $phone = $this->_db->escape($phone);
        $email = $this->_db->escape($email);
        $site = $this->_db->escape($site);
        //$categories

        $cid = Session::instance()->get('cid');
        $q=<<<SQL
INSERT INTO trainer
(`company_id`,
`description`,
`email`,
`name`,
`phone`,
`site`)
VALUES
(
$cid,
$description,
$email,
$name,
$phone,
$site
);

SQL;
        $this->_db->query(Database::INSERT,$q);
    }

    //-------- Update functions --------------

    public function editTrainer($tid, $trainer) {
        /**
         * @var $name
         * @var $description
         * @var $phone
         * @var $email
         * @var $site
         */
        extract($trainer);
        $name = $this->_db->escape($name);
        $description = $this->_db->escape($description);
        $phone = $this->_db->escape($phone);
        $email = $this->_db->escape($email);
        $site = $this->_db->escape($site);


    $q=<<<SQL
UPDATE trainer
SET
`description` = $description,
`email` = $email,
`name` = $name,
`phone` = $phone,
`site` = $site
WHERE id=$tid;
SQL;
        $this->_db->query(Database::UPDATE, $q);
    }
    //-------- Delete functions --------------

    public function deleteTrainer($tid) {
        $cid=Session::instance()->get('cid');
        $q=<<<SQL
UPDATE trainer
SET `active`='0' WHERE `id`=$tid;
SQL;
        if($this->isCompanyTrainer($tid)) {
            $this->_db->query(Database::UPDATE, $q);
            return true;
        } else {
            return false;
        }

    }

    //-------- Mix functions --------------
    public function isCompanyTrainer($tid) {
        $cid=Session::instance()->get('cid');
        $q = "SELECT * FROM trainer WHERE id=$tid";
        $result = $this->_db->query(Database::SELECT, $q, true);
        $result = $result[0];
        return ($result->company_id==$cid);

    }

}
