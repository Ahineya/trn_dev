<?php
/**
 * @author Pavel Evsegneev
 * Date: 22.01.13
 * Time: 12:52
 */
class Controller_Login extends Controller_Front
{
    public function action_index() {
        if(Auth::instance()->logged_in()) {
            $this->request->redirect('/my');
        } else {
            $this->template->content = View::factory('login/index', array(
                'test' => 'hello'
            ));
        }
    }

    public function action_registration() {
        $cities = Func::getCities();
        $this->template->content = View::factory('login/registration', array(
            'cities' => $cities
        ));
    }

    public function action_register() {
        $m = Model::factory('companies');
        $m->createCompany();
    }

    // Метод разлогивания
    public function action_logout()
    {
        // Пытаемся разлогиниться
        if (Auth::instance()->logout())
        {
            Session::instance()->restart();
            // Если получается, то предлагаем снова залогиниться
            return $this->request->redirect('/login');
        }
        else
        {
            // А иначе - пишем что не удалось
            echo 'logout failed';
        }
    }

    // Метод логина
    public function action_login()
    {
        // Проверям, вдруг пользователь уже зашел
        if(Auth::instance()->logged_in())
        {
            // И если это так, то отправляем его сразу на страницу пользователей
            return $this->request->redirect('/logged');
        }

        // Если же пользователь не зашел, но данные на страницу пришли, то:
        if ($_POST)
        {
            $user = ORM::factory('user');
            // в $status помещаем результат функции login
            $status = Auth::instance()->login($_POST['email'], $_POST['password'], true);
            // Если логин успешен, то
            if ($status)
            {
                $company = Model::factory('login')->getUserCompany(Auth::instance()->get_user()->id);
                Session::instance()->set('company_name',$company->name);
                Session::instance()->set('cid',$company->id);
                Session::instance()->set('uid',Auth::instance()->get_user()->id);
                $this->request->redirect('/my');
            }
            else
            {
                // Иначе ничего не получилось, пишем failed
                echo 'failed';
            }
        }
        // Грузим view логина
        $this->template->content = View::factory('login/index');
    }
}
