<?php
/**
 * @author Pavel Evsegneev
 * Date: 25.01.13
 * Time: 10:55
 */
class Controller_My extends Controller_Front
{
    public function action_index() {

        if(Auth::instance()->logged_in()) {
            $cid=Session::instance()->get('cid');
            $trainings = Model::factory('trainings')->getTrainingsByCompany($cid);
            $trainers = Model::factory('trainers')->getTrainersByCompany($cid);
            $news = Model::factory('news')->getNewsByCompany($cid);
            $articles = Model::factory('articles')->getArticlesByCompany($cid);

            $this->template->controls = View::factory('my/control', array(
                'active' => 'my',
            ));

            $this->template->content = View::factory(
                'my/index',
                array('title' => 'Company',
                      'trainings' => $trainings,
                      'trainers' => $trainers,
                      'news' => $news,
                      'articles' => $articles,
                ));
        } else {
            $this->request->redirect('/login');
        }
    }

    public function action_trainings() {
            if(Auth::instance()->logged_in()) {
            $cid=Session::instance()->get('cid');

            $trainings = Model::factory('trainings')->getTrainingsByCompany($cid);

            $this->template->controls = View::factory('my/control', array(
                'active' => 'trainings',
            ));

            $this->template->content = View::factory(
                'my/trainings',
                array('title' => 'Company',
                    'trainings' => $trainings,
                ));
        } else {
            $this->request->redirect('/login');
        }
    }

    public function action_trainers() {

        if(Auth::instance()->logged_in()) {
            $cid=Session::instance()->get('cid');
            $trainers = Model::factory('trainers')->getTrainersByCompany($cid);

            $this->template->controls = View::factory('my/control', array(
                'active' => 'trainers',
            ));

            $this->template->content = View::factory(
                'my/trainers',
                array('title' => 'Company',
                    'trainers' => $trainers,
                ));
        } else {
            $this->request->redirect('/login');
        }
    }

    public function action_news() {

        if(Auth::instance()->logged_in()) {
            $cid=Session::instance()->get('cid');
            $news = Model::factory('news')->getNewsByCompany($cid);

            $this->template->controls = View::factory('my/control', array(
                'active' => 'news',
            ));

            $this->template->content = View::factory(
                'my/news',
                array('title' => 'Company',
                    'news' => $news,
                ));
        } else {
            $this->request->redirect('/login');
        }
    }

    public function action_articles() {

        if(Auth::instance()->logged_in()) {
            $cid=Session::instance()->get('cid');
            $articles = Model::factory('articles')->getArticlesByCompany($cid);

            $this->template->controls = View::factory('my/control', array(
                'active' => 'articles',
            ));

            $this->template->content = View::factory(
                'my/articles',
                array('title' => 'Company',
                    'articles' => $articles,
                ));
        } else {
            $this->request->redirect('/login');
        }
    }

}
