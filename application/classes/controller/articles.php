<?php
/**
 * @author Pavel Evsegneev
 * Date: 18.01.13
 * Time: 15:37
 */
class Controller_Articles extends Controller_Front
{
    public function action_index() {
        $id = $this->request->param('id');

        /** @var Model_Articles $m */
        $m = Model::factory('articles');

        if(empty($id)) {

            $articles = $m->getArticles();

            $this->template->content = View::factory(
                'articles/index',
                array('title' => 'Trn-dev',
                    'articles' => $articles,
                ));
        } else {

            $article = $m->getArticle($id);
            if($article) {
            $this->template->content = View::factory(
                'articles/article',
                array('title' => 'Trn-dev',
                    'article' => $article,
                ));
            } else {
                //TODO: Add 404 page
            }
        }
    }
}
