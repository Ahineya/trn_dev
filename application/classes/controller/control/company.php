<?php
/**
 * @author Pavel Evsegneev
 * Date: 21.01.13
 * Time: 14:58
 */
class Controller_Control_Company extends Controller_Front
{
    public function action_index() {
        $this->template->content = View::factory(
            'control/company/index',
            array('title' => 'Company'));
    }
}
