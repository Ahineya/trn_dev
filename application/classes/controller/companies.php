<?php
/**
 * @author Pavel Evsegneev
 * Date: 18.01.13
 * Time: 13:48
 */
class Controller_Companies extends Controller_Front
{
    public function action_index() {
        $id = $this->request->param('id');

        /** @var Model_Companies $m */
        $m = Model::factory('companies');

        if(empty($id)) {

            $companies = $m->getCompanies();

            $this->template->content = View::factory(
                'companies/index',
                array('title' => 'Trn-dev',
                    'companies' => $companies,
                ));
        } else {

            $company = $m->getCompany($id);
            $this->template->content = View::factory(
                'companies/company',
                array('title' => 'Trn-dev',
                    'company' => $company,
                ));
        }
    }
}
