<?php
/**
 * @author Pavel Evsegneev
 * Date: 17.01.13
 * Time: 14:03
 */
class Controller_Trainings extends Controller_Front
{
    public function action_index() {

        $id = $this->request->param('id');

        /** @var Model_Trainings $m */
        $m = Model::factory('trainings');

        if(empty($id)) {

            $trainings = $m->getTrainings();

            $this->template->content = View::factory(
                'trainings/index',
                array('title' => 'Trn-dev',
                      'trainings' => $trainings,
                ));
        } else {

            $training = $m->getTraining($id);
            if($training) {
                $this->template->content = View::factory(
                    'trainings/training',
                    array('title' => 'Trn-dev',
                        'training' => $training,
                    ));
            } else {
                //TODO: 404
            }
        }
    }
}
