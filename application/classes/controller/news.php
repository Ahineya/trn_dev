<?php
/**
 * @author Pavel Evsegneev
 * Date: 21.01.13
 * Time: 12:55
 */
class Controller_News extends Controller_Front
{
    public function action_index() {

        $id = $this->request->param('id');

        /** @var Model_News $m */
        $m = Model::factory('news');

        if(empty($id)) {

            $news = $m->getNews();

            $this->template->content = View::factory(
                'news/index',
                array('title' => 'Trn-dev',
                    'news' => $news,
                ));
        } else {

            $news_item = $m->getNewsItem($id);
            if($news_item) {
            $this->template->content = View::factory(
                'news/news_item',
                array('title' => 'Trn-dev',
                    'news_item' => $news_item,
                ));
            } else {
                //TODO: 404
            }
        }
    }
}
