<?php
/**
 * @author Pavel Evsegneev
 * Date: 05.02.13
 * Time: 12:18
 */


class Controller_My_Edit extends Controller_Front
{
    public function before() {
        parent::before();
        $cid=Session::instance()->get('cid');
        if($cid=="") {
            $this->request->redirect('/login');
        }
    }

    public function action_news() {
        $nid = (int)$this->request->param('id');
        if(!(Model::factory('news')->isCompanyNews($nid))) {
            $this->request->redirect('/my/news');
        }
        if(!$_POST) {
            $trainers = Model::factory('trainers')->getTrainersByCompany(Session::instance()->get('cid'));
            $news = Model::factory('news')->getNewsItem($nid);

            $this->template->content = View::factory('my/edit/news')
                ->bind('trainers', $trainers)
                ->bind('news', $news)
                ->bind('nid', $nid);
        } else {
            $_POST = array_map('trim', $_POST);

            $v = Validation::factory($_POST)
                ->rule('caption','not_empty')
                ->rule('introduction', 'not_empty')
                ->rule('text', 'not_empty');

            if($v->check()) {
                Model::factory('news')->editNews($nid, $_POST);
                $this->template->message = Kohana::message('success', 'edit_news');
                $this->template->message_type ="success";
            } else {
                $errors = $v->errors('errors');

                foreach($errors as $e=>$val) {
                    $this->template->message .= $val."<br>";
                    $this->template->message_type ="error";
                }

                $trainers = Model::factory('trainers')->getTrainersByCompany(Session::instance()->get('cid'));
                $news = Model::factory('news')->getNewsItem($nid);

                $this->template->content = View::factory('my/edit/news')
                    ->bind('trainers', $trainers)
                    ->bind('news', $news)
                    ->bind('nid', $nid);
            }
        }
    }

    public function action_article() {
        if(!(Model::factory('articles')->isCompanyArticle($this->request->param('id')))) {
            $this->request->redirect('/my/articles');
        }
        if(!($_POST)) {
            $categories = Func::getCategories();
            $article = Model::factory('articles')->getArticle($this->request->param('id'));
            $article_cat = Model::factory('articles')->getArticleCategories($this->request->param('id'));

            //Debug::vars($article_cat[3]);

            $aid = $this->request->param('id');
            $cid = Session::instance()->get('cid');
            $trainers = Model::factory('trainers')->getTrainersByCompany($cid);
            $this->template->content = View::factory('my/edit/articles')
                ->bind('categories', $categories)
                ->bind('article', $article)
                ->bind('article_cat', $article_cat)
                ->bind('trainers', $trainers)
                ->bind('aid', $aid);
        } else {
            $v = Validation::factory($_POST)
                ->rule('caption','not_empty')
                ->rule('introduction', 'not_empty')
                ->rule('text', 'not_empty');

            if($v->check()) {
                Model::factory('articles')->editArticle($this->request->param('id'), $_POST);
                $this->template->message = Kohana::message('success', 'edit_article');
                $this->template->message_type ="success";
            } else {
                $errors = $v->errors('errors');

                foreach($errors as $e=>$val) {
                    $this->template->message .= $val."<br>";
                    $this->template->message_type ="error";
            }


            $this->template->content = View::factory('my/edit/articles', array(
                'caption' => $_POST['caption'],
                'introduction' => $_POST['introduction'],
                'text' => $_POST['text'],
                ));
            }
        }
    }

    public function action_trainer() {
        $tid = $this->request->param('id');
        if(!(Model::factory('trainers')->isCompanyTrainer($tid))) {
            $this->request->redirect('/my/trainers');
        }
        if(!$_POST) {
            $trainer = Model::factory('trainers')->getTrainer($tid);

            $this->template->content = View::factory('my/edit/trainers')
                ->bind('trainer', $trainer)
                ->bind('tid', $tid);
        } else {
            if ( !preg_match('/^http:\/\//i', $_POST['site']) and ($_POST['site']!="") ) {
                $_POST['site'] = "http://".$_POST['site'];
            }
            $_POST = array_map('trim', $_POST);

            $v = Validation::factory($_POST)
                ->rule('name','not_empty')
                ->rule('site','url')
                ->rule('email','email');

            if($v->check()) {
                Model::factory('trainers')->editTrainer($tid, $_POST);
                $this->template->message = Kohana::message('success', 'edit_trainer');
                $this->template->message_type ="success";
            } else {
                $errors = $v->errors('errors');

                foreach($errors as $e=>$val) {
                    $this->template->message .= $val."<br>";
                    $this->template->message_type ="error";
                }

                $this->template->content = View::factory('my/edit/trainers', array(
                    'name' => $_POST['name'],
                ));
            }
        }
    }


    public function action_training() {
        $step = $this->request->param('step');
        $tid = $this->request->param('id');

        if(!(Model::factory('trainings')->isCompanyTraining($tid))) {
            $this->request->redirect('/my/trainers');
        }

        switch ($step) {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            default:
                $training = Model::factory('trainings')->getTraining($tid);
                $categories = Model::factory('trainings')->getTrainingCategories($tid);
                    //echo Debug::vars($training);

                $this->template->content = View::factory('my/edit/training/main')
                    ->bind('training', $training)
                    ->bind('categories', $categories);
        }

    }


}
