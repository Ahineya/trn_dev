<?php
/**
 * @author Pavel Evsegneev
 * Date: 25.01.13
 * Time: 17:04
 */


class Controller_My_Delete extends Controller_Front
{

    public function before() {
        parent::before();
        $cid=Session::instance()->get('cid');
        if($cid=="") {
            $this->request->redirect('/login');
        }
    }

    public function action_news() {
        $nid = (int)$this->request->param('id');
        if($nid=="") {
            $this->request->redirect('/my/news');
        } else {
            if(Model::factory('news')->deleteNews($this->request->param('id'))) {
                $this->template->message = Kohana::message('success', 'delete_news');
                $this->template->message_type ="success";
            } else {
                $this->template->message = Kohana::message('errors', 'delete_news');
                $this->template->message_type ="error";
            };
        }
    }

    public function action_article() {
        $aid = (int)$this->request->param('id');
        if($aid=="") {
            $this->request->redirect('/my/articles');
        } else {
            if(Model::factory('articles')->deleteArticle($this->request->param('id'))) {
                $this->template->message = Kohana::message('success', 'delete_article');
                $this->template->message_type ="success";
            } else {
                $this->template->message = Kohana::message('errors', 'delete_article');
                $this->template->message_type ="error";
            };
        }
    }


    public function action_trainer() {
        $tid = (int)$this->request->param('id');
        if($tid=="") {
            $this->request->redirect('/my/trainers');
        } else {
            if(Model::factory('trainers')->deleteTrainer($this->request->param('id'))) {
                $this->template->message = Kohana::message('success', 'delete_trainer');
                $this->template->message_type ="success";
            } else {
                $this->template->message = Kohana::message('errors', 'delete_trainer');
                $this->template->message_type ="error";
            };
        }
    }

    public function action_training() {
        $tid = (int)$this->request->param('id');
        if($tid=="") {
            $this->request->redirect('/my/trainings');
        } else {
            if(Model::factory('trainings')->deleteTraining($this->request->param('id'))) {
                $this->template->message = Kohana::message('success', 'delete_training');
                $this->template->message_type ="success";
            } else {
                $this->template->message = Kohana::message('errors', 'delete_training');
                $this->template->message_type ="error";
            };
        }
    }

}
