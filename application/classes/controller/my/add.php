<?php
/**
 * @author Pavel Evsegneev
 * Date: 25.01.13
 * Time: 15:16
 */


class Controller_My_Add extends Controller_Front
{

    public function before() {
        parent::before();
        $cid=Session::instance()->get('cid');
        if($cid=="") {
            $this->request->redirect('/login');
        }
    }

    public function action_news() {
        if(!$_POST) {
            $trainers = Model::factory('trainers')->getTrainersByCompany(Session::instance()->get('cid'));
            $this->template->content = View::factory('my/add/news')->bind('trainers', $trainers);
        } else {
            $_POST = array_map('trim', $_POST);

            $v = Validation::factory($_POST)
                 ->rule('caption','not_empty')
                 ->rule('introduction', 'not_empty')
                 ->rule('text', 'not_empty');

            if($v->check()) {
                Model::factory('news')->addNews($_POST);
                $this->template->message = Kohana::message('success', 'add_news');
                $this->template->message_type ="success";
            } else {
                $errors = $v->errors('errors');

                foreach($errors as $e=>$val) {
                    $this->template->message .= $val."<br>";
                    $this->template->message_type ="error";
                }

                $this->template->content = View::factory('my/add/news', array(
                    'caption' => $_POST['caption'],
                    'introduction' => $_POST['introduction'],
                    'text' => $_POST['text'],
                ));
            }
        }
    }

    public function action_article() {
        if(!$_POST) {
            $trainers = Model::factory('trainers')->getTrainersByCompany(Session::instance()->get('cid'));
            $categories = Func::getCategories();
            $this->template->content = View::factory('my/add/articles', array(
                'categories' => $categories,
                'trainers' => $trainers
            ));
        } else {
            //$_POST = array_map('trim', $_POST);

            $v = Validation::factory($_POST)
                ->rule('caption','not_empty')
                ->rule('introduction', 'not_empty')
                ->rule('text', 'not_empty');

            if($v->check()) {
                Model::factory('articles')->addArticle($_POST);
                $this->template->message = Kohana::message('success', 'add_article');
                $this->template->message_type ="success";
            } else {
                $errors = $v->errors('errors');

                foreach($errors as $e=>$val) {
                    $this->template->message .= $val."<br>";
                    $this->template->message_type ="error";
                }


                $this->template->content = View::factory('my/add/articles', array(
                    'caption' => $_POST['caption'],
                    'introduction' => $_POST['introduction'],
                    'text' => $_POST['text'],
                ));
            }

        }
    }

    public function action_trainer() {
        if(!$_POST) {
            $this->template->content = View::factory('my/add/trainers', array(
            ));
        } else {
            if ( !preg_match('/^http:\/\//i', $_POST['site']) and ($_POST['site']!="") ) {
                $_POST['site'] = "http://".$_POST['site'];
            }
            $_POST = array_map('trim', $_POST);

            $v = Validation::factory($_POST)
                ->rule('name','not_empty')
                ->rule('site','url')
                ->rule('email','email');

            if($v->check()) {
                Model::factory('trainers')->addTrainer($_POST);
                $this->template->message = Kohana::message('success', 'add_trainer');
                $this->template->message_type ="success";
            } else {
                $errors = $v->errors('errors');

                foreach($errors as $e=>$val) {
                    $this->template->message .= $val."<br>";
                    $this->template->message_type ="error";
                }

                $this->template->content = View::factory('my/add/trainers', array(
                    'name' => $_POST['name'],
                ));
            }
        }
    }

    public function action_training() {
        $step = $this->request->param('step');

        switch($step) {
            case 2:
                //Session::instance()->delete('add_training_process');

                //echo Debug::vars($_POST);

                if(Session::instance()->get('add_training_step2')!='true') {
                    Session::instance()->set('add_training_step2','true');
                    if($this->request->method('post')) {

                        $v = Validation::factory($_POST)
                            ->rule('name','not_empty')
                            ->rule('form','not_empty')
                            ->rule('phone','not_empty')
                            ->rule('email','not_empty');

                        if($v->check()) {
                            $last_id = Model::factory('trainings')->addTraining($_POST);
                            Session::instance()->set('last_id', $last_id);
                            $type=Arr::get($_POST, 'type');
                            $this->template->content = View::factory('my/add/training/step2')
                                ->bind('type', $type)
                                ->bind('last_id', $last_id);
                        } else {
                            $errors = $v->errors('errors');

                            foreach($errors as $e=>$val) {
                                $this->template->message .= $val."<br>";
                                $this->template->message_type ="error";
                            }

                            $categories = Func::getCategories();
                            $trainers = Model::factory('trainers')->getTrainersByCompany(Session::instance()->get('cid'));
                            $this->template->content = View::factory('my/add/training/step1')
                                ->bind('categories', $categories)
                                ->bind('trainers', $trainers)
                                ->bind('_POST', $_POST);

                            Session::instance()->delete('add_training_step2');
                        }

                    }
                } else {
                    $type=Arr::get($_POST, 'type');
                    $this->template->content = View::factory('my/add/training/step2')
                        ->bind('type', $type);
                }

                break;
            case 3:
                if(Session::instance()->get('add_training_step3')!='true') {
                    if($this->request->method('post')) {
                            $last_id = Session::instance()->get('last_id');
                            Model::factory('trainings')->addTraining($_POST, $last_id, 2);
                            $type=Arr::get($_POST, 'type');
                            $this->template->content = View::factory('my/add/training/step2')
                                ->bind('type', $type);
                    }
                    Session::instance()->set('add_training_step3','true');
                } else {
                    $cities = Func::getCities();
                    $this->template->content = View::factory('my/add/training/step3')
                        ->bind('cities',$cities);
                }
                break;
            case "final":

                if($this->request->method('post')) {
                    $last_id = Session::instance()->get('last_id');
                    Model::factory('trainings')->addTraining($_POST, $last_id, 3);
                    $this->template->content = View::factory('my/add/training/final');
                    $this->template->message = Kohana::message('success', 'add_training');
                    $this->template->message_type ="success";

                } else {
                    //TODO: 404
                }

                break;
            default:
                Session::instance()->delete('add_training_step2');
                Session::instance()->delete('add_training_step3');
                Session::instance()->delete('last_id');
                $categories = Func::getCategories();
                $trainers = Model::factory('trainers')->getTrainersByCompany(Session::instance()->get('cid'));
                $this->template->content = View::factory('my/add/training/step1')
                    ->bind('categories', $categories)
                    ->bind('trainers', $trainers);
        }
    }

}

