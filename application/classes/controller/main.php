<?php
/**
 * @author Pavel Evsegneev
 * Date: 17.01.13
 * Time: 13:46
 */
class Controller_Main extends Controller_Front {

    public function action_index()
    {
        $this->template->sidebar = 'test';
        $this->template->content = View::factory(
            'main/index',
            array('title' => 'Trn-dev',
                  ));
    }

}
