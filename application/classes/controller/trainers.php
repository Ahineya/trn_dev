<?php
/**
 * @author Pavel Evsegneev
 * Date: 18.01.13
 * Time: 11:33
 */
class Controller_Trainers extends Controller_Front
{
    public function action_index() {
        $id = $this->request->param('id');

        /** @var Model_Trainers $m */
        $m = Model::factory('trainers');

        if(empty($id)) {

            $trainers = $m->getTrainers();

            $this->template->content = View::factory(
                'trainers/index',
                array('title' => 'Trn-dev',
                    'trainers' => $trainers,
                ));
        } else {

            $trainer = $m->getTrainer($id);

            if($trainer) {

            $this->template->content = View::factory(
                'trainers/trainer',
                array('title' => 'Trn-dev',
                    'trainer' => $trainer,
                ));

            } else {
                //TODO: 404
            }
        }
    }
}
