<?php
/**
 * @author Pavel Evsegneev
 * Date: 18.01.13
 * Time: 14:44
 */
class Func
{

    public static function getCities() {
        return DB::query(Database::SELECT, "SELECT * FROM city")->execute();
    }
    public static function getCategories() {
        return DB::query(Database::SELECT, "SELECT * FROM category ORDER BY name ASC")->execute();
    }
    public static function date2sql($date) {
        return date("Y-m-d H:i:s",strtotime($date));
    }
}
