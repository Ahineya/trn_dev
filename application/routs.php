<?php
/**
 * List of routs to be included in bootstrap
 *
 * Author: Anis uddin Ahmad <anisniit@gmail.com>
 * Created On: 3/30/12 6:41 PM
 */



Route::set('my_delete','my/delete/<action>(/<id>)')
    ->defaults(array(
    'directory'  => 'my',
    'controller' => 'delete',
    'action'     => 'index',
));

Route::set('my_add','my/add/<action>(/<step>)')
    ->defaults(array(
    'directory'  => 'my',
    'controller' => 'add',
    'action'     => 'index',
));

Route::set('my_edit','my/edit/<action>(/<id>(/<step>))')
    ->defaults(array(
    'directory'  => 'my',
    'controller' => 'edit',
    'action'     => 'index',
));

Route::set('my','my(/<action>)')
    ->defaults(array(
    'controller' => 'my',
    'action'     => 'index',
));

Route::set('logout','logout')
    ->defaults(array(
    'controller' => 'login',
    'action'     => 'logout',
));

Route::set('login','login(/<action>)')
    ->defaults(array(
    'controller' => 'login',
    'action'     => 'index',
));

Route::set('register','registration(/<action>)')
    ->defaults(array(
    'controller' => 'login',
    'action'     => 'registration',
));

Route::set('control', 'control')
    ->defaults(array(
    'directory'  => 'control',
    'controller' => 'main',
    'action'     => 'index',
));

Route::set('control-company', 'control/company')
    ->defaults(array(
    'directory'  => 'control',
    'controller' => 'company',
    'action'     => 'index',
));


Route::set('all', '<controller>(/<id>)')
    ->defaults(array(
    'controller' => 'main',
    'action'     => 'index',
));


Route::set('default', '(<controller>(/<action>(/<id>(/<title>))))')
	->defaults(array(
		'controller' => 'main',
		'action'     => 'index',
	));

Route::set('catch_all', '<path>', array('path' => '.+'))
	->defaults(array(
		'controller' => 'errors',
		'action'     => '404',
	));