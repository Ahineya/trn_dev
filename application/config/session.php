<?php

return array(
    'cookie' => array(
        'name' => 'session_cookie',
        'encrypted' => TRUE,
        'lifetime' => 1209600,
    ),
    'native' => array(
        'name' => 'session_native',
        'encrypted' => TRUE,
        'lifetime' => 1209600,
    ),
    'database' => array(
        'name' => 'session_database',
        'encrypted' => TRUE,
        'lifetime' => 1209600,
        'group' => 'default',
        'table' => 'sessions',
    ),
    'gc' => 500,
);

?>